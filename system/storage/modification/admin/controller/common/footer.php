<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

		$data['text_footer'] = $this->language->get('text_footer');

		if ($this->user->isLogged() && isset($this->request->get['token']) && ($this->request->get['token'] == $this->session->data['token'])) {
		/* NeoSeo SMS Notify - begin */
		$data['sms_notify'] = false;
		if ($this->config->get('neoseo_sms_notify_status') == 1) {
			$data['sendSmsModal'] = $this->load->controller('marketing/neoseo_sms_notify');
			$data['sms_notify'] = true;
		}
		/* NeoSeo SMS Notify - end */
			$data['text_version'] = sprintf($this->language->get('text_version'), VERSION);
		} else {
			$data['text_version'] = '';
		}
		
		return $this->load->view('common/footer', $data);
	}
}
