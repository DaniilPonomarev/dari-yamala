<?php
// HTTP
define('HTTP_SERVER', 'http://dari-yamala.cw97450.tmweb.ru/admin/');
define('HTTP_CATALOG', 'http://dari-yamala.cw97450.tmweb.ru/');

// HTTPS
define('HTTPS_SERVER', 'http://dari-yamala.cw97450.tmweb.ru/admin/');
define('HTTPS_CATALOG', 'http://dari-yamala.cw97450.tmweb.ru/');

// DIR
define('DIR_APPLICATION', '/home/c/cw97450/dari_yamala/public_html/admin/');
define('DIR_SYSTEM', '/home/c/cw97450/dari_yamala/public_html/system/');
define('DIR_IMAGE', '/home/c/cw97450/dari_yamala/public_html/image/');
define('DIR_LANGUAGE', '/home/c/cw97450/dari_yamala/public_html/admin/language/');
define('DIR_TEMPLATE', '/home/c/cw97450/dari_yamala/public_html/admin/view/template/');
define('DIR_CONFIG', '/home/c/cw97450/dari_yamala/public_html/system/config/');
define('DIR_CACHE', '/home/c/cw97450/dari_yamala/public_html/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/c/cw97450/dari_yamala/public_html/system/storage/download/');
define('DIR_LOGS', '/home/c/cw97450/dari_yamala/public_html/system/storage/logs/');
define('DIR_MODIFICATION', '/home/c/cw97450/dari_yamala/public_html/system/storage/modification/');
define('DIR_UPLOAD', '/home/c/cw97450/dari_yamala/public_html/system/storage/upload/');
define('DIR_CATALOG', '/home/c/cw97450/dari_yamala/public_html/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'cw97450_daryya');
define('DB_PASSWORD', 'cw97450_daryya');
define('DB_DATABASE', 'cw97450_daryya');
define('DB_PORT', '3306');
define('DB_PREFIX', 'dy_');
