<?php

// Heading
$_['heading_title'] = 'NeoSeo SMS Информер';

// Button
$_['button_send'] = 'Отправить сообщение';

// Entry
$_['entry_header_phone'] = 'Номер телефона (в международном формате)';
$_['entry_header_message'] = 'Смс-сообщение';

// Error
$_['error_message'] = 'Введите сообщение';
$_['error_permission'] = 'У Вас нет прав для управления этим модулем! Перейдите в раздел Система - Пользователи - Группы пользователи и добавьте права на просмотр и редактирование marketing/neoseo_sms_notify';
