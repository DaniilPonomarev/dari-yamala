<?php
/**
* @author dev-opencart.com <mydevopencart@gmail.com>
*/

// Heading
$_['heading_title']       	   = '<strong style="color:#41637d">DEV-OPENCART.COM —</strong> Персонализованные шаблоны <a href="https://dev-opencart.com" target="_blank" title="Dev-opencart.com - Модули и шаблоны для Opencart"><img style="margin-left:15px;height:35px;margin-top:10px;margin-bottom:10px;" src="https://dev-opencart.com/logob.svg" alt="Dev-opencart.com - Модули и шаблоны для Opencart"/></a>';
$_['text_module']			   = 'Модули';
$_['text_unregistered'] 	   = 'Незарегистрированные';
$_['text_edit']                = 'Редактировать';

//Module Types
$_['module_type1']             = 'Шаблон для категории';
$_['module_type2']             = 'Шаблон для товаров';
$_['module_type3']             = 'Шаблон для информационных страниц';
$_['module_type4']             = 'Шаблон для производителя';
$_['module_type5']             = 'Шаблон для всех товаров из определенной категории (Только с SEO URL)';
$_['module_type6']             = 'Шаблон для всех товаров определенного производителя';

//Entry
$_['entry_module_type']        = 'Тип шаблона';
$_['entry_category']           = 'Категории';
$_['entry_category_help']      = 'Если оставить пустым, то будет выбран шаблон для всех категорий.';
$_['entry_customer_group']     = 'Группы пользователей';
$_['entry_customer_group_help']= 'Если оставить пустым, то будет выбран шаблон для всех групп пользователей.';
$_['entry_information']        = 'Информационные страницы';
$_['entry_information_help']   = 'Если оставить пустым, то будет выбран шаблон для всех информационных страниц.';
$_['entry_manufacturer']       = 'Производители';
$_['entry_manufacturer_help']  = 'Если оставить пустым, то будет выбран шаблон для всех производителей.';
$_['entry_product']            = 'Товары (автозаполнение)';
$_['entry_product_help'] 	   = 'Если оставить пустым, то будет выбран шаблон для всех товаров.';
$_['entry_template'] 		   = 'Файл нового шаблона:';
$_['entry_template_addon']     = 'Тема: %s';
$_['entry_template_help']      = 'Например, product/category_wo_bg';

//Buttons
$_['button_check_file']        = 'Проверить путь';
$_['button_add_module']        = 'Добавить модуль';

//Success
$_['text_success'] 			   = 'Настройки модуля успешно сохранены.';

// Error
$_['error_permission']         = 'У Вас нет прав для изменения модуля персонализованных шаблонов!';
$_['error_max_input_vars']     = 'Превышен лимит <a href="http://devjew.com/2014/05/16/unknown-input-variables-exceeded-1000-opencart/" target="_blank">max_input_vars</a>';
$_['text_empty_field']    	   = 'Заполните все поля!';

//ajax messages
$_['ajax_success']             = 'Файл %s найден';
$_['ajax_warning']             = 'Файл %s не найден';

?>