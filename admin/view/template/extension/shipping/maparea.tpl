<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-maparea" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if (!$maparea_apikey) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> Для отображения карты укажите ваш ключ API Яндекс.Карт. <a href="https://developer.tech.yandex.ru/" target="_blank">Получить ключ</a>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
	<?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-maparea" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li><a href="#common" data-toggle="tab">Общие настройки</a></li>
            <li class="active"><a href="#maps" data-toggle="tab">Районы и цены</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane" id="common">
			
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-tax-class"><?php echo $entry_tax_class; ?></label>
				<div class="col-sm-10">
				  <select name="maparea_tax_class_id" id="input-tax-class" class="form-control">
					<option value="0"><?php echo $text_none; ?></option>
					<?php foreach ($tax_classes as $tax_class) { ?>
					<?php if ($tax_class['tax_class_id'] == $maparea_tax_class_id) { ?>
					<option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
					<?php } ?>
					<?php } ?>
				  </select>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-geo-zone"><?php echo $entry_geo_zone; ?></label>
				<div class="col-sm-10">
				  <select name="maparea_geo_zone_id" id="input-geo-zone" class="form-control">
					<option value="0"><?php echo $text_all_zones; ?></option>
					<?php foreach ($geo_zones as $geo_zone) { ?>
					<?php if ($geo_zone['geo_zone_id'] == $maparea_geo_zone_id) { ?>
					<option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
					<?php } ?>
					<?php } ?>
				  </select>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
				<div class="col-sm-10">
				  <select name="maparea_status" id="input-status" class="form-control">
					<?php if ($maparea_status) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				  </select>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-sort-order">Ключ API Яндекс.Карт:</label>
				<div class="col-sm-10">
				  <input type="text" name="maparea_apikey" value="<?php echo $maparea_apikey; ?>" id="input-apikey" class="form-control" />
				  <a href="https://developer.tech.yandex.ru/" target="_blank">получить ключ</a>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
				<div class="col-sm-10">
				  <input type="text" name="maparea_sort_order" value="<?php echo $maparea_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_show_map; ?></label>
				<div class="col-sm-10">
				  <select name="maparea_show_map" class="form-control">
				  <option value="1"<?php echo $maparea_show_map ? ' selected' : ''; ?>><?php echo $text_yes; ?></option>
				  <option value="0"<?php echo !$maparea_show_map ? ' selected' : ''; ?>><?php echo $text_no; ?></option>
				  </select>
				  <span style="color: red;">Для отображения карты требуется добавить JavaScript на страницу оформления заказа:</span>
				  <code>&lt;script src="https://api-maps.yandex.ru/2.1/?apikey=<b>Ваш-ключ-API</b>&lang=ru_RU"&gt;&lt;/script&gt;</code>
				</div>
			  </div>

            </div>
			
            <div class="tab-pane active" id="maps">
			<div style="color: red;">Район без координат - это доставка по-умолчанию, на всю географическую зону. Убедитесь, что у него максимальный порядок сортировки.</div><br>
			<?php if (isset($maparea_map['points'])) foreach ($maparea_map['points'] as $i=>$pt) { ?>
			<div class="area-block row" style="margin-bottom: 20px;">
				<div id="map_<?php echo $i; ?>" class="col-sm-8 map" style="height: 400px;"></div>
				<div class="col-sm-4">
				  <div class="form-group">
					<label class="col-sm-6 control-label" for="input-sort-order"><?php echo $entry_on; ?></label>
					<div class="col-sm-3">
					  <select name="maparea_map[on][]" class="form-control">
					  <option value="1"<?php echo isset($maparea_map['on'][$i]) && $maparea_map['on'][$i] ? ' selected' : ''; ?>><?php echo $text_yes; ?></option>
					  <option value="0"<?php echo !isset($maparea_map['on'][$i]) || !$maparea_map['on'][$i] ? ' selected' : ''; ?>><?php echo $text_no; ?></option>
					  </select>
					</div>
					<div class="col-sm-3">
					  <a title="<?php echo $text_delete_area; ?>" class="delete-area btn btn-danger pull-right" href="javascript:void(0);"><i class="fa fa-trash"></i></a>
					</div>
				  </div>
				  <div class="form-group">
					<label class="col-sm-6 control-label"><?php echo $entry_area_name; ?></label>
					<div class="col-sm-6">
					  <input type="text" name="maparea_map[name][]" value="<?php echo $maparea_map['name'][$i]; ?>" class="form-control" />
					</div>
				  </div>
				  <div class="form-group">
					<label class="col-sm-6 control-label"><?php echo $entry_price; ?></label>
					<div class="col-sm-6">
					  <input type="text" name="maparea_map[price][]" value="<?php echo $maparea_map['price'][$i]; ?>" class="form-control" />
					</div>
				  </div>
				  <div class="form-group">
					<label class="col-sm-6 control-label"><?php echo $entry_points; ?></label>
					<div class="col-sm-6">
					  <textarea name="maparea_map[points][]" class="area-points form-control" rows="3"><?php echo $maparea_map['points'][$i]; ?></textarea>
					</div>
				  </div>
				  <div class="form-group">
					<label class="col-sm-6 control-label"><?php echo $entry_sort_order; ?></label>
					<div class="col-sm-6">
					  <input type="text" name="maparea_map[sort_order][]" value="<?php echo $maparea_map['sort_order'][$i]; ?>" class="form-control" />
					</div>
				  </div>
				</div>
			</div>
			<?php } ?>
			
			<a id="addMap" class="btn btn-primary" href="javascript:void(0);"><?php echo $entry_add_area; ?></a>
			<div id="map_seed" class="area-block row" style="display: none; margin-bottom: 20px;">
				<div class="col-sm-8 map" style="height: 400px;"></div>
				<div class="col-sm-4">
				  <div class="form-group">
					<label class="col-sm-6 control-label" for="input-sort-order"><?php echo $entry_on; ?></label>
					<div class="col-sm-3">
					  <select name="maparea_map[on][]" class="form-control">
					  <option value="1"><?php echo $text_yes; ?></option>
					  <option value="0"><?php echo $text_no; ?></option>
					  </select>
					</div>
					<div class="col-sm-3">
					  <a title="<?php echo $text_delete_area; ?>" class="delete-area btn btn-danger pull-right" href="javascript:void(0);"><i class="fa fa-trash"></i></a>
					</div>
				  </div>
				  <div class="form-group">
					<label class="col-sm-6 control-label"><?php echo $entry_area_name; ?></label>
					<div class="col-sm-6">
					  <input type="text" name="maparea_map[name][]" value="" class="form-control" />
					</div>
				  </div>
				  <div class="form-group">
					<label class="col-sm-6 control-label"><?php echo $entry_price; ?></label>
					<div class="col-sm-6">
					  <input type="text" name="maparea_map[price][]" value="" class="form-control" />
					</div>
				  </div>
				  <div class="form-group">
					<label class="col-sm-6 control-label"><?php echo $entry_points; ?></label>
					<div class="col-sm-6">
					  <textarea name="maparea_map[points][]" class="area-points form-control" rows="3"></textarea>
					</div>
				  </div>
				  <div class="form-group">
					<label class="col-sm-6 control-label"><?php echo $entry_sort_order; ?></label>
					<div class="col-sm-6">
					  <input type="text" name="maparea_map[sort_order][]" value="" class="form-control" />
					</div>
				  </div>
				</div>
			</div>
			
            </div>
			
          </div>
		  <input type="hidden" name="maparea_set" value="1" />
        </form>  
      </div>
    </div>
  </div>
</div>
<script src="https://api-maps.yandex.ru/2.1/?apikey=<?php echo $maparea_apikey; ?>&lang=ru_RU"></script>
<script type="text/javascript"><!--
$(document).ready(function() {
	
$('#maps').on('click', '.delete-area', function() {
	$(this).parents('.area-block').remove();
	return false;
})

$('#form-maparea').submit(function() {
	$('#map_seed').remove();
})
	
ymaps.ready(init);

function init() {
	var i = <?php echo isset($maparea_map['points']) ? count($maparea_map['points']) : 0; ?>;
	$('#addMap').click(function() {
		var map_div = $('#map_seed').clone();
		map_div.removeAttr('id');
		$('#addMap').before(map_div);
		i++;
		map_div.find('.map').attr('id', 'map_'+i.toString());
		map_div.show();
		initMap('map_'+i.toString(), []);
		return false;
	})
	
<?php if (isset($maparea_map['points'])) foreach ($maparea_map['points'] as $i=>$pt) { ?>
		initMap('map_<?php echo $i; ?>', [<?php echo $pt; ?>]);
<?php } ?>
}

function initMap(mapId, points) {
	
	
    var myMap = new ymaps.Map(mapId, {
        center: [55.73, 37.75],
        zoom: 10
    });
	var pts = [];
	var j=0;
	while (j<points.length) {
		pts.push([points[j], points[j+1]]);
		j+= 2;
	}

	var myPolygon = createPolygon(myMap, pts, mapId);
	
	if (!points.length) {
		ymaps.geolocation.get({
			// Выставляем опцию для определения положения по ip
			provider: 'yandex',
			// Карта автоматически отцентрируется по положению пользователя.
			mapStateAutoApply: true
		}).then(function (result) {
			myMap.geoObjects.add(result.geoObjects);
			
		});
	}
	else {
		var bounds = myPolygon.geometry.getBounds();
		myMap.setBounds(bounds, { checkZoomRange: false });
	}
}

function createPolygon(myMap, pts, mapId) {
	// Создаем многоугольник
	var myPolygon = new ymaps.Polygon([pts], {}, {
		// Курсор в режиме добавления новых вершин.
		editorDrawingCursor: "crosshair",
		// Максимально допустимое количество вершин.
		editorMaxPoints: 1000,
		// Цвет заливки.
		fillColor: '#00FF004F',
		// Цвет обводки.
		strokeColor: '#0000FF',
		// Ширина обводки.
		strokeWidth: 2
	});

	myPolygon.geometry.events.add('change', function () {
	   $('#'+mapId).parents('.area-block').find('.area-points').val(myPolygon.geometry.getCoordinates().toString()); 
	});

	// Добавляем многоугольник на карту.
	myMap.geoObjects.add(myPolygon);


	// В режиме добавления новых вершин меняем цвет обводки многоугольника.
	var stateMonitor = new ymaps.Monitor(myPolygon.editor.state);
	stateMonitor.add("drawing", function (newValue) {
		myPolygon.options.set("strokeColor", newValue ? '#FF0000' : '#0000FF');
	});

	// Включаем режим редактирования с возможностью добавления новых вершин.
	myPolygon.editor.startDrawing();
	
	return myPolygon;
}

})
//--></script>
<?php echo $footer; ?>
