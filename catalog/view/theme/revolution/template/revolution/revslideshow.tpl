<div id="revslideshow" class="owl-carousel banners" style="opacity: 1;">
	<?php if ($home_slideshows) { ?>
		<?php foreach ($home_slideshows as $home_slideshow) { ?>
			<div class="item">
				<?php if ($home_slideshow['title']) { ?>
					<span class="home_slideshow_title" style="color:#<?php echo $home_slideshow['title_color']; ?>; font-size:<?php echo $home_slideshow['title_fsize']; ?>%; top:<?php echo $home_slideshow['title_xpos']; ?>; left:<?php echo $home_slideshow['title_ypos']; ?>"><?php echo $home_slideshow['title']; ?></span>
				<?php } ?>
				<?php if ($home_slideshow['description']) { ?>
					<span class="home_slideshow_description" style="color:#<?php echo $home_slideshow['description_color']; ?>; font-size:<?php echo $home_slideshow['description_fsize']; ?>%; top:<?php echo $home_slideshow['description_xpos']; ?>; left:<?php echo $home_slideshow['description_ypos']; ?>"><?php echo $home_slideshow['description']; ?></span>
				<?php } ?>
				<?php if ($home_slideshow['link']) { ?>
					<a class="home_slideshow_link<?php if ($home_slideshow['checkbox']) { ?> popup_html_content<?php } ?>" href="<?php echo $home_slideshow['link']; ?>" style="color:#<?php echo $home_slideshow['link_title_color']; ?>; border: 1px solid #<?php echo $home_slideshow['link_title_color']; ?>; font-size:<?php echo $home_slideshow['link_title_fsize']; ?>%; top:<?php echo $home_slideshow['link_title_xpos']; ?>; left:<?php echo $home_slideshow['link_title_ypos']; ?>"><?php echo $home_slideshow['link_title']; ?></a>
				<?php } ?>
				<img src="<?php echo $home_slideshow['image']; ?>" alt="<?php echo $home_slideshow['title']; ?>" class="img-responsive" />
			</div>
		<?php } ?>
	<?php } else { ?>
		<?php foreach ($banners as $banner) { ?>
			<div class="item">
				<?php if ($banner['link']) { ?>
					<a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
				<?php } else { ?>
					<img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
				<?php } ?>
			</div>
		<?php } ?>
	<?php } ?>
</div>
<style> 
@media (min-width: 1600px) { 
	div#revslideshow {
		margin-bottom: -2.4% !important;
	}
}
@media (max-width: 767px) { 
	div#revslideshow {
		margin-bottom: -1.3% !important;
	}
}
</style>
<script><!--
	$('#revslideshow').owlCarousel({
		itemsCustom: [[0, 1], [375, 1], [750, <?php echo $slides; ?>], [970, <?php echo $slides; ?>], [1170, <?php echo $slides; ?>]],
		responsiveBaseWidth: '#revslideshow',
		stopOnHover : true,
		navigation: true,
		navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
		pagination: false,
		<?php if ($autoscroll) { ?>
			autoPlay: <?php echo $autoscroll; ?>*1000,
		<?php } ?>
	});
--></script>