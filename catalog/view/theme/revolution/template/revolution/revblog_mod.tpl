<?php if ($heading_title) { ?>
<div class="rev_slider rev_blog_mod">
<?php } else { ?>
<div class="rev_slider_2 rev_blog_mod">
<?php } ?>
	<div class="heading_h"><span class="h3"><?php echo $heading_title; ?></span><?php echo $url_all; ?></div>
	<div class="row">
		<div id="blog_mod" class="inhomeblogmod">
			<?php foreach ($blogs as $blog) { ?>
			<div class="product-layout col-lg-12">
				<div class="blog-list vertical-sreview transition">
					<div class="caption review-caption">
						<?php if ($image_status) { ?>
							<div class="image">
								<a href="<?php echo $blog['href']; ?>"><img class="img-responsive" style="border-radius: 4px; margin-bottom: 10px; box-shadow: rgb(0 0 0 / 20%) 0px 7px 19px 7px; overflow: visible;" src="<?php echo $blog['thumb']; ?>" alt="<?php echo $blog['title']; ?>" title="<?php echo $blog['title']; ?>" /></a>
							</div>
						<?php } ?>
						<div class="opisb">
							<span class="h4" style="font-size: 17px; color: #212121;"><a href="<?php echo $blog['href']; ?>"><?php echo $blog['title']; ?></a></h4>
						<?php if (isset($blog_date_status) && $blog_date_status) { ?>
							<p><?php if (!isset($category_blog_grid) || !$category_blog_grid) { ?><i class="fa fa-clock-o"></i><?php } ?><?php echo $blog['data_added']; ?></p>
						<?php } ?>
						</div>
						<?php if ($blog['description'] != '..') { ?>
						<div class="description"><?php echo $blog['description']; ?></div>
						<?php } ?>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
	<script>
		$("#blog_mod.inhomeblogmod").owlCarousel({
			responsiveBaseWidth: '#blog_mod',
			<?php if ($revtheme_home_blog['count_r']) { ?>
			itemsCustom: [[0, 1], [750, 2], [970, 3], [1170, 3]],
			<?php } else { ?>
			itemsCustom: [[0, 1], [750, 2], [970, 3], [1170, 3]],
			<?php } ?>
			mouseDrag: true,
			touchDrag: true,
			navigation: true,
			navigationText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
			<?php if (!$heading_title) { ?>
			pagination: false
			<?php } ?>
		});
		max_height_div('#blog_mod.inhomeblogmod span.h4');
		$(window).load(function() {
			max_height_div('#blog_mod.inhomeblogmod .caption.review-caption');
		});
	</script>
</div>