<?php if ($blocks) { ?>
	<div class="home_home_blocks">
		<?php foreach ($blocks as $block) { ?>
			<div class="home_block hidden-xs col-sm-4 col-md-4 col-lg-4">
				<?php if ($block['link']) { ?>
				<a href="<?php echo $block['link']; ?>" <?php if ($block['checkbox']) { ?>class="popup_html_content"<?php } ?>>
				<?php } ?>
					<div class="image <?php echo $block['style']; ?>"><?php echo $block['image']; ?></div>
					<div class="text">
						<?php if ($block['title']) { ?>
							<span class="title"><?php echo $block['title']; ?></span>
						<?php } ?>
						<?php if ($block['description']) { ?>
							<p><?php echo $block['description']; ?></p>
						<?php } ?>
					</div>
				<?php if ($block['link']) { ?>
				</a>
				<?php } ?>
			</div>
		<?php } ?>
	</div>
	<script><!--
	<?php if ($amazon && $module_class && $up_menu_height) { ?>
	$('.home_block').filter(function(i){if (!(i%3)) $(this).removeClass().addClass('home_block hidden-xs col-sm-4 col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4')})
	<?php } ?>
	max_height_div('.home_block .text');
	--></script>
<?php } ?>