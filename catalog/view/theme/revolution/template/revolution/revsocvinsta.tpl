<div class="module-instagram" style="width:<?php echo $width; ?>px">
    <div class="main">
        <div class="images">
            <?php foreach ($images as $image) { ?>
				<a href="https://www.instagram.com/<?php echo $username; ?>/" target="_blank">
					<img src="<?php echo $image; ?>" class="img-responsive" alt="" />
				</a>
            <?php } ?>
        </div>
    </div>
</div>