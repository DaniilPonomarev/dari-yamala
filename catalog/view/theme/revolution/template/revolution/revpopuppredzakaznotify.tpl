<div id="popup-order-okno">
	<div class="popup-heading"><?php echo $heading_title; ?></div>
	<div class="popup-center">	
		<form method="post" enctype="multipart/form-data" id="purchase-form">
		<input name="product_id" value="<?php echo $product_id; ?>" style="display: none;" type="hidden" />
		<input class="product_max" value="<?php echo $quantity; ?>" style="display: none;" type="hidden" />
		<table class="display-products-cart porder">
			<tbody>
				<tr>
					<td class="image">
						<?php if ($thumb) { ?>
							<img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
						<?php } ?>
					</td>
					<td class="name">
						<?php echo $product_name; ?>
					</td>
					<td class="totals">
						<?php if (!$special) { ?>
							<span id="main-price"><?php echo $price; ?></span>
						<?php } else { ?>
							<span id="main-price" class="spec"><?php echo $price; ?></span>
							<span id="special-price"><?php echo $special; ?></span>
						<?php } ?>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="mobile-products-cart">
			<div>
			<div class="image">
				<?php if ($thumb) { ?>
					<img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
				<?php } ?>
			</div>
			<div class="name">
				<?php echo $product_name; ?>
			</div>
			<div class="totals">
				<?php if (!$special) { ?>
					<span id="main-price" class="mobi"><?php echo $price; ?></span>
				<?php } else { ?>
					<span id="main-price" class="mobi spec"><?php echo $price; ?></span>
					<span id="special-price" class="mobi"><?php echo $special; ?></span>
				<?php } ?>
			</div>		
			</div>
		</div>
		<div class="payment-info">
			<?php if ($revtheme_predzakaz['firstname']) { ?>
			<div>
			  <label><?php if ($revtheme_predzakaz['firstname'] == 2) { ?><span class="required">*</span><?php } ?><?php echo $enter_firstname; ?></label>
			  <input name="firstname" value="<?php echo $firstname;?>" />
			</div>
			<?php } ?>
			<?php if ($revtheme_predzakaz['telephone']) { ?>
			<div>
			  <label><?php if ($revtheme_predzakaz['telephone'] == 2) { ?><span class="required">*</span><?php } ?><?php echo $enter_telephone; ?></label>
			  <input name="telephone" value="<?php echo $telephone;?>" <?php if ($telephone_mask) { ?> data-mask="<?php echo $telephone_mask;?>" <?php } ?> />
			</div>
			<?php } ?>
			<?php if ($revtheme_predzakaz['email']) { ?>
			<div>
			  <label><?php if ($revtheme_predzakaz['email'] == 2) { ?><span class="required">*</span><?php } ?><?php echo $enter_email; ?></label>
			  <input name="email" value="<?php echo $email;?>" />
			</div>
			<?php } ?>
			<?php if ($revtheme_predzakaz['comment']) { ?>
			<div>
			  <label><?php if ($revtheme_predzakaz['comment'] == 2) { ?><span class="required">*</span><?php } ?><?php echo $enter_comment; ?></label>
			  <textarea name="comment"><?php echo $comment;?></textarea>
			</div>
			<?php } ?>
		</div>
		<?php if ($text_agree_pol_konf) { ?>
			<div class="rev_pol_konf pull-right text-right">
				<span><?php echo $text_agree_pol_konf; ?></span>
				<input type="checkbox" name="agree_pol_konf" checked="checked" />
			</div>
		<?php } ?>
		</form>
	</div>
	<div class="popup-footer">
		<button class="popup_close_oneclick" onclick="$.magnificPopup.close();"><?php echo $button_shopping; ?></button>
			<a id="popup-checkout-button"><?php echo $button_checkout; ?></a>
	</div>
	<script><!--
	function masked(element, status) {
		if (status == true) {
			$('<div/>')
			.attr({ 'class':'masked' })
			.prependTo(element);
			$('<div class="masked_loading" />').insertAfter($('.masked'));
		} else {
			$('.masked').remove();
			$('.masked_loading').remove();
		}
	}
	$('#popup-checkout-button').on('click', function() {
		masked('#popup-order-okno', true);
		$.ajax({
			type: 'post',
			url:  'index.php?route=revolution/revpopuppredzakaz/make_order_notify',
			dataType: 'json',
			data: $('#purchase-form').serialize(),
			success: function(json) {
				if (json['error']) {
					if (json['error']['field']) {
						masked('#popup-order-okno', false);
						$('.text-danger').remove();
						$.each(json['error']['field'], function(i, val) {
							$('[name="' + i + '"]').addClass('error_style').after('<div class="text-danger">' + val + '</div>');
						});
					}
				} else {
					if (json['output']) {
						masked('#popup-order-okno', false);
						$('#popup-checkout-button').remove();
						$('#popup-order-okno .popup-center').html(json['output']);
					}
				}
			}
		});
	});
	//--></script>
</div>