<?php if ($heading_title) { ?>
<div class="rev_slider <?php if (!$setting['mobi_status']) { echo 'hidden-xs hidden-sm'; } ?>">
<div class="heading_h"><span class="h3"><?php echo $heading_title; ?></span></div>
<?php } else { ?>
<div class="rev_slider_2 <?php if (!$setting['mobi_status']) { echo 'hidden-xs hidden-sm'; } ?>">
<?php } ?>
	<div class="row home_catwalls">
		<?php if ($setting['group_manufs'] && $setting['group_manufs'] != 'wall' && $setting['group_manufs'] != 'slider' && $setting['categories'] != 0) { ?>
			<ul class="ul_block_home_catwalls">
				<?php foreach ($categories as $category) { ?>
					<li class="glavli">
						<div class="col-sm-12">
							<?php if ($setting['group_manufs_simbol']) { ?>
							<h3 id="<?php echo $category['name']; ?>"><?php echo $category['name']; ?></h3>
							<?php } ?>
							<div class="clearfix">
								<?php if ($category['manufacturer']) { ?>
									<?php foreach (array_chunk($category['manufacturer'], 4) as $manufacturers) { ?>
										<?php foreach ($manufacturers as $manufacturer) { ?>
										<span class="h4"><a style="text-decoration: none" href="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?></a></span>
										<?php } ?>
									<?php } ?>
								<?php } ?>
							</div>
						</div>
					</li>
				<?php } ?>
				<?php if ($setting['group_manufs_link_href'] != '' && $setting['group_manufs_link'] != '') { ?>
					<li class="glavli">
						<span class="h4" class="home_catwalls_all_href"><a style="text-decoration: none" href="<?php echo $setting['group_manufs_link_href']; ?>"><?php echo $group_manufs_link; ?> </a></span>
					</li>
				<?php } ?>
			</ul>
		<?php } else if ($setting['group_manufs'] && $setting['group_manufs'] != 'wall' && $setting['group_manufs'] != 'slider' && $setting['categories'] == 0) { ?>
			<ul class="ul_block_home_catwalls">
				<?php foreach ($categories as $category) { ?>
					<li class="glavli">
						<div class="col-sm-12">
							<div class="clearfix">
								<span class="h4" class="corencats"><a style="text-decoration: none" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></span>
								<?php if ($category['children']) { ?>	
									<?php foreach ($category['children'] as $child) { ?>
										<div>
											<a class="home_catwalls_podcat" href="<?php echo $child['href']; ?>">- <?php echo $child['name']; ?></a>
										</div>
									<?php } ?>
								<?php } ?>
							</div>
						</div>
					</li>
				<?php } ?>
				<?php if ($setting['group_manufs_link_href'] != '' && $setting['group_manufs_link'] != '') { ?>
					<li class="glavli">
						<h4 class="home_catwalls_all_href corencats"><a style="text-decoration: none" href="<?php echo $setting['group_manufs_link_href']; ?>"><?php echo $group_manufs_link; ?> </a></h4>
					</li>
				<?php } ?>
			</ul>
		<?php } else if ($setting['group_manufs'] && $setting['group_manufs'] == 'wall') { ?>
			<?php foreach ($categories as $category) { ?>
				<div class="revcatwall col-lg-4 col-md-6 col-sm-6 col-xs-6">
					<div class="product-thumb transition" style="height: auto; !important">
						<div class="row">
							<div class="hidden-xs col-sm-6">
								<div class="image"><a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" class="img-responsive" /></a></div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="caption clearfix">
									<span class="h4"><a style="text-decoration: none" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></span>
								</div>
								<?php if ($category['children']) { ?>
									<div class="aftercaption">
										<?php foreach ($category['children'] as $child) { ?>
											<div>
												<a class="home_catwalls_podcat" href="<?php echo $child['href']; ?>">- <?php echo $child['name']; ?></a>
											</div>
										<?php } ?>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		<?php } else if ($setting['group_manufs'] && $setting['group_manufs'] == 'slider') { ?>
			<div class="home_catwall_crsl">
				<?php foreach ($categories as $category) { ?>
					<div class="col-lg-12 item">
						<div class="product-thumb transition" style="height: auto; !important">
							<div class="image"><a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" class="img-responsive" /></a></div>
							<div class="caption clearfix">
								<span class="h4"><a style="text-decoration: none" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></span>
							</div> 
						</div>
					</div>
				<?php } ?>
			</div>
		<?php } else { ?>
			<?php foreach ($categories as $category) { ?>
				<div class="col-lg-6 col-md-3 col-sm-4 col-xs-6">
					<div class="product-thumb transition" style="height: auto; !important">
						<div class="image" style="width: 85%;"><a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" class="img-responsive" /></a></div>
						<div class="caption clearfix">
							<span class="h4"><a style="text-decoration: none" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></span>
						</div> 
					</div>
				</div>
			<?php } ?>
		<?php } ?>
	</div>
</div>
<script><!--
<?php if ($slider) { ?>
$(".home_catwalls .home_catwall_crsl").owlCarousel({
	responsiveBaseWidth: '.home_catwalls .home_catwall_crsl',
	<?php if ($chislo_ryad) { ?>
		<?php if ($setting_all_settings['mobil_two']) { ?>
			itemsCustom: [[0, 1], [294, 2], [375, 2], [750, 2], [970, 2], [1170, 2]],
		<?php } else { ?>
			itemsCustom: [[0, 1], [375, 1], [750, 2], [970, 2], [1170, 2]],
		<?php } ?>
	<?php } else { ?>
		<?php if ($setting_all_settings['mobil_two']) { ?>
			itemsCustom: [[0, 1], [294, 2], [375, 2], [750, 2], [970, 2], [980, 2]],
		<?php } else { ?>
			itemsCustom: [[0, 1], [375, 1], [750, 2], [970, 2], [980, 2]],
		<?php } ?>
	<?php } ?>
	mouseDrag: true,
	touchDrag: true,
	navigation: false,
	autoPlay: 2800,
	autoplayHoverPause: true,
	loop: true,
	navigationText: false,
	<?php if (!$heading_title) { ?>
	pagination: false
	<?php } ?>
});
<?php } ?>
	
var div = '.home_catwalls .caption';
var maxheight = 0;$(div).each(function(){$(this).removeAttr('style');if($(this).height() > maxheight) {maxheight = $(this).height();}});$(div).height(maxheight);

<?php if ($setting['group_manufs'] && $setting['group_manufs'] != 'wall') { ?>
	!function(n){var a={columns:4,classname:"column",min:1};n.fn.autocolumnlist=function(l){var s=n.extend({},a,l);return this.each(function(){var a=n(this).data();a&&n.each(a,function(n,a){s[n]=a});var l=n(this).find("> li"),c=l.length;if(c>0){var t=Math.ceil(c/s.columns);t<s.min&&(t=s.min);var e=0,u=t;for(i=0;i<s.columns;i++)i+1==s.columns?l.slice(e,u).wrapAll('<div class="'+s.classname+' last" />'):l.slice(e,u).wrapAll('<div class="'+s.classname+'" />'),e+=t,u+=t}})}}(jQuery);
	
var product_grid_width = $('.product-layout .product-thumb').outerWidth();
if ($(window).width() < 991) {
	$('.ul_block_home_catwalls').autocolumnlist({
		columns: 3,
		min: 1
	});
} else {
	$('.ul_block_home_catwalls').autocolumnlist({
		columns: 4,
		min: 1
	});
}	
<?php } elseif (($setting['group_manufs'] && $setting['group_manufs'] == 'wall') || ($setting['group_manufs'] && $setting['group_manufs'] == 'slider')) { ?>
max_height_div('.home_catwalls .revcatwall .product-thumb');
<?php } ?>
//--></script>
<?php if ($setting['group_manufs'] && !$setting['group_manufs_simbol']) { ?>
<style>
.ul_block_home_catwalls h4 {
padding-left: 0;
}
.ul_block_home_catwalls h4.home_catwalls_all_href {
padding-left: 15px;
}
</style>
<?php } ?>