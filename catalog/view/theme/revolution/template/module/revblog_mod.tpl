<div class="rev_slider rev_blog_mod_n_<?php echo $module; ?> rev_blog_mod_left_column<?php if (!$image_status) { ?> rev_blog_mod_noimage_status<?php } ?><?php if ($heading_title) { ?> rev_blog_mod_heading_title<?php } else { ?> rev_blog_mod_noheading_title<?php } ?><?php if ($slider) { ?> rev_blog_mod_slider<?php } ?>">
	<?php if ($heading_title) { ?>
		<div class="heading_h"><span class="h3"><?php echo $heading_title; ?></span></div>
	<?php } ?>
	<div class="row">
		<div id="blog_mod" class="modulblog_mod">
			<?php foreach ($blogs as $blog) { ?>
			<div class="col-lg-12">
				<div class="blog-list vertical-sreview transition">
					<div class="caption review-caption">
						<?php if ($image_status) { ?>
						<div class="image">
							<a href="<?php echo $blog['href']; ?>"><img class="img-responsive" src="<?php echo $blog['thumb']; ?>" alt="<?php echo $blog['title']; ?>" title="<?php echo $blog['title']; ?>" /></a>
						</div>
						<?php } ?>
						<div class="opisb">
						<span class="h4"><a href="<?php echo $blog['href']; ?>"><?php echo $blog['title']; ?></a></span>
						<?php if ($data_status) { ?><p><?php if (!isset($category_blog_grid) || !$category_blog_grid) { ?><i class="fa fa-clock-o"></i><?php } ?><?php echo $blog['data_added']; ?></p><?php } ?>
						</div>
						<div class="description"><?php echo $blog['description']; ?></div>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
	<?php if ($slider) { ?>
	<script><!--
		$(".rev_blog_mod_n_<?php echo $module; ?> .modulblog_mod").owlCarousel({
			responsiveBaseWidth: '.rev_blog_mod_n_<?php echo $module; ?> .modulblog_mod',
			itemsCustom: [[0, 1], [750, 2], [970, 3], [1170, 3]],
			mouseDrag: true,
			touchDrag: true,
			navigation: true,
			navigationText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
			<?php if ($heading_title) { ?>
			pagination: true
			<?php } else { ?>
			pagination: false
			<?php } ?>
		});
	//--></script>
	<?php } ?>
</div>