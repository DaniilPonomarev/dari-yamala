<div class = "panel panel-default revmenumodcustom">
	<?php if ($heading_title) { ?>
		<div class ="panel-heading"><?php echo $heading_title; ?></div>
	<?php } ?>
	<div class="list-group">
		<?php foreach ($blog_categories as $category) { ?>
			<?php if ($category['category_id'] == $category_id) { ?>
				<a href="<?php echo $category['href']; ?>" class="list-group-item active"><?php echo $category['title']; ?></a>
				<?php if ($category['children']) { ?>
					<?php foreach ($category['children'] as $child) { ?>
						<?php if ($child['category_id'] == $child_id) { ?>
							<a href="<?php echo $child['href']; ?>" class="list-group-item active">&nbsp;&nbsp;&nbsp;- <?php echo $child['title']; ?></a>
						<?php } else { ?>
							<a href="<?php echo $child['href']; ?>" class="list-group-item">&nbsp;&nbsp;&nbsp;- <?php echo $child['title']; ?></a>
						<?php } ?>
					<?php } ?>
				<?php } ?>
			<?php } else { ?>
				<a href="<?php echo $category['href']; ?>" class="list-group-item"><?php echo $category['title']; ?></a>
			<?php } ?>
		<?php } ?>
	</div>
</div>

