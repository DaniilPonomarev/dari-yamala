<?php echo $header; ?>
<div class="container">
<div itemscope itemtype="http://schema.org/BreadcrumbList" style="display:none;">
<?php $position = 1; foreach ($breadcrumbs as $breadcrumb) { ?>
<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
<link itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
<meta itemprop="name" content="<?php echo $breadcrumb['text']; ?>" />
<meta itemprop="position" content="<?php echo $position; ?>" />
</div>
<?php $position++; } ?>
</div>
<ul class="breadcrumb"><li class="br_ellipses" style="display: none;">...</li>
<?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
<?php if($i+1<count($breadcrumbs)) { ?><li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li><?php } else { ?><?php } ?>
<?php } ?>
<li><h1 class="inbreadcrumb"><?php echo $heading_title; ?></h1></li>
</ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <?php if ($products) { ?>
      <table class="table table-revolution">
        <thead>
          <tr>
            <td class="text-center hidden-xs"><?php echo $column_image; ?></td>
            <td class="text-left"><?php echo $column_name; ?></td>
			<?php if ($settings['revcheckout_model']) { ?>
				<td class="text-center hidden-xs"><?php echo $column_model; ?></td>
			<?php } ?>
			<?php if ($settings['revcheckout_sku']) { ?>
				<td class="text-center hidden-xs"><?php echo $column_sku; ?></td>
			<?php } ?>
            <td class="text-center hidden-xs"><?php echo $column_stock; ?></td>
            <td class="text-center hidden-xs"><?php echo $column_price; ?></td>
            <td class="text-center"><?php echo $column_action; ?></td>
          </tr>
        </thead>
        <tbody class="products_category">
          <?php foreach ($products as $product) { ?>
          <tr class="product_<?php echo $product['product_id']; ?>">
            <td class="text-center image hidden-xs"><?php if ($product['thumb']) { ?>
              <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
              <?php } ?></td>
            <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></td>
			<?php if ($settings['revcheckout_model']) { ?>
				<td class="text-center hidden-xs"><?php echo $product['model']; ?></td>
			<?php } ?>
			<?php if ($settings['revcheckout_sku']) { ?>
				<td class="text-center hidden-xs"><?php echo $product['sku']; ?></td>
			<?php } ?>
            <td class="text-center hidden-xs"><?php echo $product['stock']; ?></td>
            <td class="text-center hidden-xs"><?php if ($product['price']) { ?>
              <div class="price">
                <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?><?php echo ($product['ed_izm'] ? '<span class="ed_izm">'.$product['ed_izm'].'</span>' : ''); ?>
                <?php } else { ?>
                <span class="price-old"><?php echo $product['price']; ?></span><span class="price-new"><?php echo $product['special']; ?></span><?php echo ($product['ed_izm'] ? '<span class="ed_izm">'.$product['ed_izm'].'</span>' : ''); ?>
                <?php } ?>
              </div>
              <?php } ?></td>
            <td class="text-center wwicons">
			<?php if ($product['quantity'] > 0) { ?>
				<a onclick="get_revpopup_cart('<?php echo $product['product_id']; ?>', 'catalog_mod');" data-toggle="tooltip" title="<?php echo $button_cart; ?>"><i class="fa fa-border fa-shopping-basket"></i></a>
			<?php } else if ($predzakaz_button) { ?>
				<a onclick="get_revpopup_predzakaz('<?php echo $product['product_id']; ?>');" data-toggle="tooltip" title="<?php echo $text_predzakaz; ?>"><i class="fa fa-border fa-shopping-basket predzakaz"></i></a>
			<?php } ?>
            <a href="<?php echo $product['remove']; ?>" data-toggle="tooltip" title="<?php echo $button_remove; ?>"><i class="fa fa-trash-o"></i></a>
			</td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>