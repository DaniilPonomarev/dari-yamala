</section>
<?php if (!$setting_all_settings['all_document_width'] && $setting_all_settings['all_document_h_f_width']) { ?>
</div>
<?php } ?>
<footer>
<?php if ($f_map && $is_desctope) { ?>
	<div id="map-wrapper" class="hidden-xs">
		<div class="contact-info">
			<div class="item">
				<div class="label_fconts"><?php echo $text_contact_ourcontacts; ?></div>
				<?php if ($settings_all_settings['m_conts']) { ?>
					<p class="hid_address"><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $address; ?></p>
					<p class="hid_telephone"><i class="fa fa fa-phone" aria-hidden="true"></i><?php echo $telephone; ?></p>
					<p class="hid_email"><i class="fa fa fa-envelope" aria-hidden="true"></i><?php echo $config_email; ?></p>
				<?php } ?>
				<?php if ($settings_all_settings['dop_conts']) { ?>
					<?php if ($header_phone_number != '') { ?>
						<p class="dop_contact_tel1"><i class="fa fa fa-phone" aria-hidden="true"></i><?php echo $header_phone_cod; ?> <?php echo $header_phone_number; ?></p>
					<?php } ?>
					<?php if ($header_phone_number2 != '') { ?>
						<p class="dop_contact_tel2"><i class="fa fa fa-phone" aria-hidden="true"></i><?php echo $header_phone_cod2; ?> <?php echo $header_phone_number2; ?><p>
					<?php } ?>
					<?php if ($dop_contacts) { $i_dc = 1; ?>
						<?php foreach ($dop_contacts as $dop_contact) { ?>
							<?php if ($dop_contact['href'] != '') { ?>
								<p class="dop_contact_<?php echo $i_dc; ?>"><?php echo $dop_contact['icon']; ?><a href="<?php echo $dop_contact['href']; ?>"><?php echo $dop_contact['number']; ?></a></p>
							<?php } else { ?>
								<p class="dop_contact_<?php echo $i_dc; ?>"><?php echo $dop_contact['icon']; ?><?php echo $dop_contact['number']; ?></p>
							<?php } ?>
						<?php $i_dc++; } ?>
					<?php } ?>
				<?php } ?>
				<?php if ($settings_all_settings['soc_conts']) { ?>
					<?php if ($revtheme_footer_socs_p) { ?>
						<?php foreach ($revtheme_footer_socs_p as $revtheme_footer_soc) { ?>
							<p><i class="<?php echo $revtheme_footer_soc['image']; ?>" aria-hidden="true"></i><a href="<?php echo $revtheme_footer_soc['link']; ?>" rel="nofollow" target="_blank"><?php echo $revtheme_footer_soc['link_t']; ?></a></p>
						<?php } ?>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
		<div id="yamap">
			<?php if ($yamap) echo $yamap; ?>
		</div>
	</div>
<?php } ?>
	<?php echo $revsubscribe; ?>

	<div class="footer">

	<div class="footer_cfs_mob container">
                        <div class="row">
                            <?php if ($revtheme_custom_footer['cf_1_status']) { ?>
                                <?php if ($revtheme_dop_menus_cf || strlen($cf_1_description) > 15) { ?>
                                    <div class="col-xs-6"  style="text-align: left; padding-top: 10px;">
                                        <?php if ($revtheme_dop_menus_cf) { ?>
                                            <ul class="list-unstyled">
                                                <?php foreach ($revtheme_dop_menus_cf as $revtheme_dop_menu) { ?>
                                                    <li class="zag_dm_cf_li">
                                                        <span class="zag_dm_cf">
                                                        <?php if ($revtheme_dop_menu['href'.$config_language_id] != '') { ?>
                                                            <a href="<?php echo $revtheme_dop_menu['href'.$config_language_id]; ?>"><?php if ($revtheme_dop_menu['icontype'] == 'iconka' && $revtheme_dop_menu['dop_menu_iconka_cf'] != '') { echo '<span class="am_category_image"><i class="'.$revtheme_dop_menu['dop_menu_iconka_cf'].'"></i></span>'; } else { if ($revtheme_dop_menu['dop_menu_image_cf']) { echo '<span class="am_category_image"><img src="'.$revtheme_dop_menu['dop_menu_image_cf'].'" alt=""></span>'; } } ?><?php echo $revtheme_dop_menu['name'.$config_language_id]; ?></a>
                                                        <?php } else { ?>
                                                            <?php if ($revtheme_dop_menu['icontype'] == 'iconka' && $revtheme_dop_menu['dop_menu_iconka_cf'] != '') { echo '<span class="am_category_image"><i class="'.$revtheme_dop_menu['dop_menu_iconka_cf'].'"></i></span>'; } else { if ($revtheme_dop_menu['dop_menu_image_cf']) { echo '<span class="am_category_image"><img src="'.$revtheme_dop_menu['dop_menu_image_cf'].'" alt=""></span>'; } } ?><?php echo $revtheme_dop_menu['name'.$config_language_id]; ?>
                                                        <?php } ?>
                                                        </span>
                                                        <?php if (isset($revtheme_dop_menu['children'])) { ?>
                                                                <ul class="list-unstyled last_ul_cf">
                                                                    <?php foreach ($revtheme_dop_menu['children'] as $child) { ?>
                                                                        <li>
                                                                            <?php if ($child['href'.$config_language_id] != '') { ?>
                                                                                <a href="<?php echo $child['href'.$config_language_id]; ?>"><?php if ($child['icontype'] == 'iconka' && $child['dop_menu_iconka_cf'] != '') { echo '<span class="am_category_image"><i class="'.$child['dop_menu_iconka_cf'].'"></i></span>'; } else { if ($child['dop_menu_image_cf']) { echo '<span class="am_category_image"><img src="'.$child['dop_menu_image_cf'].'" alt=""></span>'; } } ?><?php echo $child['name'.$config_language_id]; ?></a>
                                                                            <?php } else { ?>
                                                                                <?php if ($child['icontype'] == 'iconka' && $child['dop_menu_iconka_cf'] != '') { echo '<span class="am_category_image"><i class="'.$child['dop_menu_iconka_cf'].'"></i></span>'; } else { if ($child['dop_menu_image_cf']) { echo '<span class="am_category_image"><img src="'.$child['dop_menu_image_cf'].'" alt=""></span>'; } } ?><?php echo $child['name'.$config_language_id]; ?>
                                                                            <?php } ?>
                                                                        </li>
                                                                    <?php } ?>
                                                                </ul>
                                                        <?php } ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        <?php } ?>
                                        <?php if (strlen($cf_1_description) > 15) { echo $cf_1_description; } ?>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <?php if ($revtheme_custom_footer['cf_2_status']) { ?>
                                <?php if ($revtheme_dop_menus_cf_2 || strlen($cf_2_description) > 15) { ?>
                                    <div class="col-xs-6"  style="text-align: left; padding-top: 10px;">
                                        <?php if ($revtheme_dop_menus_cf_2) { ?>
                                            <ul class="list-unstyled">
                                                <?php foreach ($revtheme_dop_menus_cf_2 as $revtheme_dop_menu) { ?>
                                                    <li class="zag_dm_cf_li">
                                                        <span class="zag_dm_cf">
                                                        <?php if ($revtheme_dop_menu['href'.$config_language_id] != '') { ?>
                                                            <a href="<?php echo $revtheme_dop_menu['href'.$config_language_id]; ?>"><?php if ($revtheme_dop_menu['icontype'] == 'iconka' && $revtheme_dop_menu['dop_menu_iconka_cf_2'] != '') { echo '<span class="am_category_image"><i class="'.$revtheme_dop_menu['dop_menu_iconka_cf_2'].'"></i></span>'; } else { if ($revtheme_dop_menu['dop_menu_image_cf_2']) { echo '<span class="am_category_image"><img src="'.$revtheme_dop_menu['dop_menu_image_cf_2'].'" alt=""></span>'; } } ?><?php echo $revtheme_dop_menu['name'.$config_language_id]; ?></a>
                                                        <?php } else { ?>
                                                            <?php if ($revtheme_dop_menu['icontype'] == 'iconka' && $revtheme_dop_menu['dop_menu_iconka_cf_2'] != '') { echo '<span class="am_category_image"><i class="'.$revtheme_dop_menu['dop_menu_iconka_cf_2'].'"></i></span>'; } else { if ($revtheme_dop_menu['dop_menu_image_cf_2']) { echo '<span class="am_category_image"><img src="'.$revtheme_dop_menu['dop_menu_image_cf_2'].'" alt=""></span>'; } } ?><?php echo $revtheme_dop_menu['name'.$config_language_id]; ?>
                                                        <?php } ?>
                                                        </span>
                                                        <?php if (isset($revtheme_dop_menu['children'])) { ?>
                                                                <ul class="list-unstyled last_ul_cf">
                                                                    <?php foreach ($revtheme_dop_menu['children'] as $child) { ?>
                                                                        <li>
                                                                            <?php if ($child['href'.$config_language_id] != '') { ?>
                                                                                <a href="<?php echo $child['href'.$config_language_id]; ?>"><?php if ($child['icontype'] == 'iconka' && $child['dop_menu_iconka_cf_2'] != '') { echo '<span class="am_category_image"><i class="'.$child['dop_menu_iconka_cf_2'].'"></i></span>'; } else { if ($child['dop_menu_image_cf_2']) { echo '<span class="am_category_image"><img src="'.$child['dop_menu_image_cf_2'].'" alt=""></span>'; } } ?><?php echo $child['name'.$config_language_id]; ?></a>
                                                                            <?php } else { ?>
                                                                                <?php if ($child['icontype'] == 'iconka' && $child['dop_menu_iconka_cf_2'] != '') { echo '<span class="am_category_image"><i class="'.$child['dop_menu_iconka_cf_2'].'"></i></span>'; } else { if ($child['dop_menu_image_cf_2']) { echo '<span class="am_category_image"><img src="'.$child['dop_menu_image_cf_2'].'" alt=""></span>'; } } ?><?php echo $child['name'.$config_language_id]; ?>
                                                                            <?php } ?>
                                                                        </li>
                                                                    <?php } ?>
                                                                </ul>
                                                        <?php } ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        <?php } ?>
                                        <?php if (strlen($cf_2_description) > 15) { echo $cf_2_description; } ?>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <?php if ($revtheme_custom_footer['cf_3_status']) { ?>
                                <?php if ($revtheme_dop_menus_cf_3 || strlen($cf_3_description) > 15) { ?>
                                    <div class="col-xs-12" style="text-align: left; padding-top: 10px;">
                                        <?php if ($revtheme_dop_menus_cf_3) { ?>
                                            <ul class="list-unstyled">
                                                <?php foreach ($revtheme_dop_menus_cf_3 as $revtheme_dop_menu) { ?>
                                                    <li class="zag_dm_cf_li">
                                                        <span class="zag_dm_cf">
                                                        <?php if ($revtheme_dop_menu['href'.$config_language_id] != '') { ?>
                                                            <a href="<?php echo $revtheme_dop_menu['href'.$config_language_id]; ?>"><?php if ($revtheme_dop_menu['icontype'] == 'iconka' && $revtheme_dop_menu['dop_menu_iconka_cf_3'] != '') { echo '<span class="am_category_image"><i class="'.$revtheme_dop_menu['dop_menu_iconka_cf_3'].'"></i></span>'; } else { if ($revtheme_dop_menu['dop_menu_image_cf_3']) { echo '<span class="am_category_image"><img src="'.$revtheme_dop_menu['dop_menu_image_cf_3'].'" alt=""></span>'; } } ?><?php echo $revtheme_dop_menu['name'.$config_language_id]; ?></a>
                                                        <?php } else { ?>
                                                            <?php if ($revtheme_dop_menu['icontype'] == 'iconka' && $revtheme_dop_menu['dop_menu_iconka_cf_3'] != '') { echo '<span class="am_category_image"><i class="'.$revtheme_dop_menu['dop_menu_iconka_cf_3'].'"></i></span>'; } else { if ($revtheme_dop_menu['dop_menu_image_cf_3']) { echo '<span class="am_category_image"><img src="'.$revtheme_dop_menu['dop_menu_image_cf_3'].'" alt=""></span>'; } } ?><?php echo $revtheme_dop_menu['name'.$config_language_id]; ?>
                                                        <?php } ?>
                                                        </span>
                                                        <?php if (isset($revtheme_dop_menu['children'])) { ?>
                                                                <ul class="list-unstyled last_ul_cf">
                                                                    <?php foreach ($revtheme_dop_menu['children'] as $child) { ?>
                                                                        <li>
                                                                            <?php if ($child['href'.$config_language_id] != '') { ?>
                                                                                <a href="<?php echo $child['href'.$config_language_id]; ?>"><?php if ($child['icontype'] == 'iconka' && $child['dop_menu_iconka_cf_3'] != '') { echo '<span class="am_category_image"><i class="'.$child['dop_menu_iconka_cf_3'].'"></i></span>'; } else { if ($child['dop_menu_image_cf_3']) { echo '<span class="am_category_image"><img src="'.$child['dop_menu_image_cf_3'].'" alt=""></span>'; } } ?><?php echo $child['name'.$config_language_id]; ?></a>
                                                                            <?php } else { ?>
                                                                                <?php if ($child['icontype'] == 'iconka' && $child['dop_menu_iconka_cf_3'] != '') { echo '<span class="am_category_image"><i class="'.$child['dop_menu_iconka_cf_3'].'"></i></span>'; } else { if ($child['dop_menu_image_cf_3']) { echo '<span class="am_category_image"><img src="'.$child['dop_menu_image_cf_3'].'" alt=""></span>'; } } ?><?php echo $child['name'.$config_language_id]; ?>
                                                                            <?php } ?>
                                                                        </li>
                                                                    <?php } ?>
                                                                </ul>
                                                        <?php } ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        <?php } ?>
                                        <?php if (strlen($cf_3_description) > 15) { echo $cf_3_description; } ?>
                                    </div>
                                <?php } ?>
							<?php } ?>
							
							<?php if ($revtheme_custom_footer['cf_4_status']) { ?>
							<?php if ($revtheme_dop_menus_cf_4 || strlen($cf_4_description) > 15) { ?>
								<div class="col-xs-12" style="text-align: left; padding-top: 5%;">
									<?php if ($revtheme_dop_menus_cf_4) { ?>
										<ul class="list-unstyled">
											<?php foreach ($revtheme_dop_menus_cf_4 as $revtheme_dop_menu) { ?>
												<li class="zag_dm_cf_li">
													<span class="zag_dm_cf">
													<?php if ($revtheme_dop_menu['href'.$config_language_id] != '') { ?>
														<a href="<?php echo $revtheme_dop_menu['href'.$config_language_id]; ?>"><?php if ($revtheme_dop_menu['icontype'] == 'iconka' && $revtheme_dop_menu['dop_menu_iconka_cf_4'] != '') { echo '<span class="am_category_image"><i class="'.$revtheme_dop_menu['dop_menu_iconka_cf_4'].'"></i></span>'; } else { if ($revtheme_dop_menu['dop_menu_image_cf_4']) { echo '<span class="am_category_image"><img src="'.$revtheme_dop_menu['dop_menu_image_cf_4'].'" alt=""></span>'; } } ?><?php echo $revtheme_dop_menu['name'.$config_language_id]; ?></a>
													<?php } else { ?>
														<?php if ($revtheme_dop_menu['icontype'] == 'iconka' && $revtheme_dop_menu['dop_menu_iconka_cf_4'] != '') { echo '<span class="am_category_image"><i class="'.$revtheme_dop_menu['dop_menu_iconka_cf_4'].'"></i></span>'; } else { if ($revtheme_dop_menu['dop_menu_image_cf_4']) { echo '<span class="am_category_image"><img src="'.$revtheme_dop_menu['dop_menu_image_cf_4'].'" alt=""></span>'; } } ?><?php echo $revtheme_dop_menu['name'.$config_language_id]; ?>
													<?php } ?>
													</span>
													<?php if (isset($revtheme_dop_menu['children'])) { ?>
															<ul class="list-unstyled last_ul_cf">
																<?php foreach ($revtheme_dop_menu['children'] as $child) { ?>
																	<li>
																		<?php if ($child['href'.$config_language_id] != '') { ?>
																			<a href="<?php echo $child['href'.$config_language_id]; ?>"><?php if ($child['icontype'] == 'iconka' && $child['dop_menu_iconka_cf_4'] != '') { echo '<span class="am_category_image"><i class="'.$child['dop_menu_iconka_cf_4'].'"></i></span>'; } else { if ($child['dop_menu_image_cf_4']) { echo '<span class="am_category_image"><img src="'.$child['dop_menu_image_cf_4'].'" alt=""></span>'; } } ?><?php echo $child['name'.$config_language_id]; ?></a>
																		<?php } else { ?>
																			<?php if ($child['icontype'] == 'iconka' && $child['dop_menu_iconka_cf_4'] != '') { echo '<span class="am_category_image"><i class="'.$child['dop_menu_iconka_cf_4'].'"></i></span>'; } else { if ($child['dop_menu_image_cf_4']) { echo '<span class="am_category_image"><img src="'.$child['dop_menu_image_cf_4'].'" alt=""></span>'; } } ?><?php echo $child['name'.$config_language_id]; ?>
																		<?php } ?>
																	</li>
																<?php } ?>
															</ul>
													<?php } ?>
												</li>
											<?php } ?>
										</ul>
									<?php } ?>
									<?php if (strlen($cf_4_description) > 15) { echo $cf_4_description; } ?>
								</div>
							<?php } ?>
						<?php } ?>
                        </div>
					</div>


		<div class="container">
			<div class="row">
				<?php $dops_class = "not_dops_snot_dops_s"; if (!$revtheme_custom_footer['dops_pc_status']) { $dops_class = "not_dops_s hidden-md hidden-lg"; } ?>
				<?php if ($is_mobile || $dops_class == "not_dops_snot_dops_s") { ?>
				<div class="<?php echo $dops_class; ?>">
					<?php if ($informations || $revtheme_footer_links) { ?>
						<?php $class_informations = 'col-sm-12' ?>
						<?php if (($informations || $revtheme_footer_links) && $revtheme_footer_socs) { $class_informations = 'col-sm-8'; } ?>
						<div class="footer_links <?php echo $class_informations; ?> col-xs-12 <?php if ($setting_all_settings['mobile_header'] == '2') { ?>hidden-xs<?php } ?>">
							<?php if ($informations) { ?>
								<?php foreach ($informations as $information) { ?>
									<a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a>
								<?php } ?>
							<?php } ?>
							<?php if ($revtheme_footer_links) { ?>
								<?php foreach ($revtheme_footer_links as $revtheme_footer_link) { ?>
									<a href="<?php echo $revtheme_footer_link['link']; ?>"><?php echo $revtheme_footer_link['title']; ?></a>
								<?php } ?>
							<?php } ?>
						</div>
					<?php } ?>
					<?php if ($revtheme_footer_socs) { ?>
						<?php $class_footer_socs = 'col-sm-12' ?>
						<?php if (($informations || $revtheme_footer_links) && $revtheme_footer_socs) { $class_footer_socs = 'col-sm-4'; } ?>
						<div class="soc_s <?php echo $class_footer_socs; ?> col-xs-12 <?php if ($setting_all_settings['mobile_header'] == '2') { ?>hidden-xs<?php } ?>">
							<?php foreach ($revtheme_footer_socs as $revtheme_footer_soc) { ?>
								<a href="<?php echo $revtheme_footer_soc['link']; ?>" rel="nofollow" target="_blank"><i class="<?php echo $revtheme_footer_soc['image']; ?>" data-toggle="tooltip" title="<?php echo $revtheme_footer_soc['title']; ?>"></i></a>
							<?php } ?>
						</div>
					<?php } ?>
				</div>
				<?php } ?>
				<?php if ($revtheme_custom_footer['status']&& $is_desctope) { ?>
					<div class="footer_cfs hidden-sm">
						<?php if ($revtheme_custom_footer['cf_1_status']) { ?>
							<?php if ($revtheme_dop_menus_cf || strlen($cf_1_description) > 15) { ?>
								<div <?php echo $cf_1_width; ?>>
									<?php if ($revtheme_dop_menus_cf) { ?>
										<ul class="list-unstyled">
											<?php foreach ($revtheme_dop_menus_cf as $revtheme_dop_menu) { ?>
												<li class="zag_dm_cf_li">
													<span class="zag_dm_cf">
													<?php if ($revtheme_dop_menu['href'.$config_language_id] != '') { ?>
														<a href="<?php echo $revtheme_dop_menu['href'.$config_language_id]; ?>"><?php if ($revtheme_dop_menu['icontype'] == 'iconka' && $revtheme_dop_menu['dop_menu_iconka_cf'] != '') { echo '<span class="am_category_image"><i class="'.$revtheme_dop_menu['dop_menu_iconka_cf'].'"></i></span>'; } else { if ($revtheme_dop_menu['dop_menu_image_cf']) { echo '<span class="am_category_image"><img src="'.$revtheme_dop_menu['dop_menu_image_cf'].'" alt=""></span>'; } } ?><?php echo $revtheme_dop_menu['name'.$config_language_id]; ?></a>
													<?php } else { ?>
														<?php if ($revtheme_dop_menu['icontype'] == 'iconka' && $revtheme_dop_menu['dop_menu_iconka_cf'] != '') { echo '<span class="am_category_image"><i class="'.$revtheme_dop_menu['dop_menu_iconka_cf'].'"></i></span>'; } else { if ($revtheme_dop_menu['dop_menu_image_cf']) { echo '<span class="am_category_image"><img src="'.$revtheme_dop_menu['dop_menu_image_cf'].'" alt=""></span>'; } } ?><?php echo $revtheme_dop_menu['name'.$config_language_id]; ?>
													<?php } ?>
													</span>
													<?php if (isset($revtheme_dop_menu['children'])) { ?>
															<ul class="list-unstyled last_ul_cf">
																<?php foreach ($revtheme_dop_menu['children'] as $child) { ?>
																	<li>
																		<?php if ($child['href'.$config_language_id] != '') { ?>
																			<a href="<?php echo $child['href'.$config_language_id]; ?>"><?php if ($child['icontype'] == 'iconka' && $child['dop_menu_iconka_cf'] != '') { echo '<span class="am_category_image"><i class="'.$child['dop_menu_iconka_cf'].'"></i></span>'; } else { if ($child['dop_menu_image_cf']) { echo '<span class="am_category_image"><img src="'.$child['dop_menu_image_cf'].'" alt=""></span>'; } } ?><?php echo $child['name'.$config_language_id]; ?></a>
																		<?php } else { ?>
																			<?php if ($child['icontype'] == 'iconka' && $child['dop_menu_iconka_cf'] != '') { echo '<span class="am_category_image"><i class="'.$child['dop_menu_iconka_cf'].'"></i></span>'; } else { if ($child['dop_menu_image_cf']) { echo '<span class="am_category_image"><img src="'.$child['dop_menu_image_cf'].'" alt=""></span>'; } } ?><?php echo $child['name'.$config_language_id]; ?>
																		<?php } ?>
																	</li>
																<?php } ?>
															</ul>
													<?php } ?>
												</li>
											<?php } ?>
										</ul>
									<?php } ?>
									<?php if (strlen($cf_1_description) > 15) { echo $cf_1_description; } ?>
								</div>
							<?php } ?>
						<?php } ?>
						<?php if ($revtheme_custom_footer['cf_2_status']) { ?>
							<?php if ($revtheme_dop_menus_cf_2 || strlen($cf_2_description) > 15) { ?>
								<div <?php echo $cf_2_width; ?>>
									<?php if ($revtheme_dop_menus_cf_2) { ?>
										<ul class="list-unstyled">
											<?php foreach ($revtheme_dop_menus_cf_2 as $revtheme_dop_menu) { ?>
												<li class="zag_dm_cf_li">
													<span class="zag_dm_cf">
													<?php if ($revtheme_dop_menu['href'.$config_language_id] != '') { ?>
														<a href="<?php echo $revtheme_dop_menu['href'.$config_language_id]; ?>"><?php if ($revtheme_dop_menu['icontype'] == 'iconka' && $revtheme_dop_menu['dop_menu_iconka_cf_2'] != '') { echo '<span class="am_category_image"><i class="'.$revtheme_dop_menu['dop_menu_iconka_cf_2'].'"></i></span>'; } else { if ($revtheme_dop_menu['dop_menu_image_cf_2']) { echo '<span class="am_category_image"><img src="'.$revtheme_dop_menu['dop_menu_image_cf_2'].'" alt=""></span>'; } } ?><?php echo $revtheme_dop_menu['name'.$config_language_id]; ?></a>
													<?php } else { ?>
														<?php if ($revtheme_dop_menu['icontype'] == 'iconka' && $revtheme_dop_menu['dop_menu_iconka_cf_2'] != '') { echo '<span class="am_category_image"><i class="'.$revtheme_dop_menu['dop_menu_iconka_cf_2'].'"></i></span>'; } else { if ($revtheme_dop_menu['dop_menu_image_cf_2']) { echo '<span class="am_category_image"><img src="'.$revtheme_dop_menu['dop_menu_image_cf_2'].'" alt=""></span>'; } } ?><?php echo $revtheme_dop_menu['name'.$config_language_id]; ?>
													<?php } ?>
													</span>
													<?php if (isset($revtheme_dop_menu['children'])) { ?>
															<ul class="list-unstyled last_ul_cf">
																<?php foreach ($revtheme_dop_menu['children'] as $child) { ?>
																	<li>
																		<?php if ($child['href'.$config_language_id] != '') { ?>
																			<a href="<?php echo $child['href'.$config_language_id]; ?>"><?php if ($child['icontype'] == 'iconka' && $child['dop_menu_iconka_cf_2'] != '') { echo '<span class="am_category_image"><i class="'.$child['dop_menu_iconka_cf_2'].'"></i></span>'; } else { if ($child['dop_menu_image_cf_2']) { echo '<span class="am_category_image"><img src="'.$child['dop_menu_image_cf_2'].'" alt=""></span>'; } } ?><?php echo $child['name'.$config_language_id]; ?></a>
																		<?php } else { ?>
																			<?php if ($child['icontype'] == 'iconka' && $child['dop_menu_iconka_cf_2'] != '') { echo '<span class="am_category_image"><i class="'.$child['dop_menu_iconka_cf_2'].'"></i></span>'; } else { if ($child['dop_menu_image_cf_2']) { echo '<span class="am_category_image"><img src="'.$child['dop_menu_image_cf_2'].'" alt=""></span>'; } } ?><?php echo $child['name'.$config_language_id]; ?>
																		<?php } ?>
																	</li>
																<?php } ?>
															</ul>
													<?php } ?>
												</li>
											<?php } ?>
										</ul>
									<?php } ?>
									<?php if (strlen($cf_2_description) > 15) { echo $cf_2_description; } ?>
								</div>
							<?php } ?>
						<?php } ?>
						<?php if ($revtheme_custom_footer['cf_3_status']) { ?>
							<?php if ($revtheme_dop_menus_cf_3 || strlen($cf_3_description) > 15) { ?>
								<div <?php echo $cf_3_width; ?>>
									<?php if ($revtheme_dop_menus_cf_3) { ?>
										<ul class="list-unstyled">
											<?php foreach ($revtheme_dop_menus_cf_3 as $revtheme_dop_menu) { ?>
												<li class="zag_dm_cf_li">
													<span class="zag_dm_cf">
													<?php if ($revtheme_dop_menu['href'.$config_language_id] != '') { ?>
														<a href="<?php echo $revtheme_dop_menu['href'.$config_language_id]; ?>"><?php if ($revtheme_dop_menu['icontype'] == 'iconka' && $revtheme_dop_menu['dop_menu_iconka_cf_3'] != '') { echo '<span class="am_category_image"><i class="'.$revtheme_dop_menu['dop_menu_iconka_cf_3'].'"></i></span>'; } else { if ($revtheme_dop_menu['dop_menu_image_cf_3']) { echo '<span class="am_category_image"><img src="'.$revtheme_dop_menu['dop_menu_image_cf_3'].'" alt=""></span>'; } } ?><?php echo $revtheme_dop_menu['name'.$config_language_id]; ?></a>
													<?php } else { ?>
														<?php if ($revtheme_dop_menu['icontype'] == 'iconka' && $revtheme_dop_menu['dop_menu_iconka_cf_3'] != '') { echo '<span class="am_category_image"><i class="'.$revtheme_dop_menu['dop_menu_iconka_cf_3'].'"></i></span>'; } else { if ($revtheme_dop_menu['dop_menu_image_cf_3']) { echo '<span class="am_category_image"><img src="'.$revtheme_dop_menu['dop_menu_image_cf_3'].'" alt=""></span>'; } } ?><?php echo $revtheme_dop_menu['name'.$config_language_id]; ?>
													<?php } ?>
													</span>
													<?php if (isset($revtheme_dop_menu['children'])) { ?>
															<ul class="list-unstyled last_ul_cf">
																<?php foreach ($revtheme_dop_menu['children'] as $child) { ?>
																	<li>
																		<?php if ($child['href'.$config_language_id] != '') { ?>
																			<a href="<?php echo $child['href'.$config_language_id]; ?>"><?php if ($child['icontype'] == 'iconka' && $child['dop_menu_iconka_cf_3'] != '') { echo '<span class="am_category_image"><i class="'.$child['dop_menu_iconka_cf_3'].'"></i></span>'; } else { if ($child['dop_menu_image_cf_3']) { echo '<span class="am_category_image"><img src="'.$child['dop_menu_image_cf_3'].'" alt=""></span>'; } } ?><?php echo $child['name'.$config_language_id]; ?></a>
																		<?php } else { ?>
																			<?php if ($child['icontype'] == 'iconka' && $child['dop_menu_iconka_cf_3'] != '') { echo '<span class="am_category_image"><i class="'.$child['dop_menu_iconka_cf_3'].'"></i></span>'; } else { if ($child['dop_menu_image_cf_3']) { echo '<span class="am_category_image"><img src="'.$child['dop_menu_image_cf_3'].'" alt=""></span>'; } } ?><?php echo $child['name'.$config_language_id]; ?>
																		<?php } ?>
																	</li>
																<?php } ?>
															</ul>
													<?php } ?>
												</li>
											<?php } ?>
										</ul>
									<?php } ?>
									<?php if (strlen($cf_3_description) > 15) { echo $cf_3_description; } ?>
								</div>
							<?php } ?>
						<?php } ?>
						<?php if ($revtheme_custom_footer['cf_4_status']) { ?>
							<?php if ($revtheme_dop_menus_cf_4 || strlen($cf_4_description) > 15) { ?>
								<div <?php echo $cf_4_width; ?>>
									<?php if ($revtheme_dop_menus_cf_4) { ?>
										<ul class="list-unstyled">
											<?php foreach ($revtheme_dop_menus_cf_4 as $revtheme_dop_menu) { ?>
												<li class="zag_dm_cf_li">
													<span class="zag_dm_cf">
													<?php if ($revtheme_dop_menu['href'.$config_language_id] != '') { ?>
														<a href="<?php echo $revtheme_dop_menu['href'.$config_language_id]; ?>"><?php if ($revtheme_dop_menu['icontype'] == 'iconka' && $revtheme_dop_menu['dop_menu_iconka_cf_4'] != '') { echo '<span class="am_category_image"><i class="'.$revtheme_dop_menu['dop_menu_iconka_cf_4'].'"></i></span>'; } else { if ($revtheme_dop_menu['dop_menu_image_cf_4']) { echo '<span class="am_category_image"><img src="'.$revtheme_dop_menu['dop_menu_image_cf_4'].'" alt=""></span>'; } } ?><?php echo $revtheme_dop_menu['name'.$config_language_id]; ?></a>
													<?php } else { ?>
														<?php if ($revtheme_dop_menu['icontype'] == 'iconka' && $revtheme_dop_menu['dop_menu_iconka_cf_4'] != '') { echo '<span class="am_category_image"><i class="'.$revtheme_dop_menu['dop_menu_iconka_cf_4'].'"></i></span>'; } else { if ($revtheme_dop_menu['dop_menu_image_cf_4']) { echo '<span class="am_category_image"><img src="'.$revtheme_dop_menu['dop_menu_image_cf_4'].'" alt=""></span>'; } } ?><?php echo $revtheme_dop_menu['name'.$config_language_id]; ?>
													<?php } ?>
													</span>
													<?php if (isset($revtheme_dop_menu['children'])) { ?>
															<ul class="list-unstyled last_ul_cf">
																<?php foreach ($revtheme_dop_menu['children'] as $child) { ?>
																	<li>
																		<?php if ($child['href'.$config_language_id] != '') { ?>
																			<a href="<?php echo $child['href'.$config_language_id]; ?>"><?php if ($child['icontype'] == 'iconka' && $child['dop_menu_iconka_cf_4'] != '') { echo '<span class="am_category_image"><i class="'.$child['dop_menu_iconka_cf_4'].'"></i></span>'; } else { if ($child['dop_menu_image_cf_4']) { echo '<span class="am_category_image"><img src="'.$child['dop_menu_image_cf_4'].'" alt=""></span>'; } } ?><?php echo $child['name'.$config_language_id]; ?></a>
																		<?php } else { ?>
																			<?php if ($child['icontype'] == 'iconka' && $child['dop_menu_iconka_cf_4'] != '') { echo '<span class="am_category_image"><i class="'.$child['dop_menu_iconka_cf_4'].'"></i></span>'; } else { if ($child['dop_menu_image_cf_4']) { echo '<span class="am_category_image"><img src="'.$child['dop_menu_image_cf_4'].'" alt=""></span>'; } } ?><?php echo $child['name'.$config_language_id]; ?>
																		<?php } ?>
																	</li>
																<?php } ?>
															</ul>
													<?php } ?>
												</li>
											<?php } ?>
										</ul>
									<?php } ?>
									<?php if (strlen($cf_4_description) > 15) { echo $cf_4_description; } ?>
								</div>
							<?php } ?>
						<?php } ?>
						<?php if ($revtheme_custom_footer['cf_5_status']) { ?>
							<?php if ($revtheme_dop_menus_cf_5 || strlen($cf_5_description) > 15) { ?>
								<div <?php echo $cf_5_width; ?>>
									<?php if ($revtheme_dop_menus_cf_5) { ?>
										<ul class="list-unstyled">
											<?php foreach ($revtheme_dop_menus_cf_5 as $revtheme_dop_menu) { ?>
												<li class="zag_dm_cf_li">
													<span class="zag_dm_cf">
													<?php if ($revtheme_dop_menu['href'.$config_language_id] != '') { ?>
														<a href="<?php echo $revtheme_dop_menu['href'.$config_language_id]; ?>"><?php if ($revtheme_dop_menu['icontype'] == 'iconka' && $revtheme_dop_menu['dop_menu_iconka_cf_5'] != '') { echo '<span class="am_category_image"><i class="'.$revtheme_dop_menu['dop_menu_iconka_cf_5'].'"></i></span>'; } else { if ($revtheme_dop_menu['dop_menu_image_cf_5']) { echo '<span class="am_category_image"><img src="'.$revtheme_dop_menu['dop_menu_image_cf_5'].'" alt=""></span>'; } } ?><?php echo $revtheme_dop_menu['name'.$config_language_id]; ?></a>
													<?php } else { ?>
														<?php if ($revtheme_dop_menu['icontype'] == 'iconka' && $revtheme_dop_menu['dop_menu_iconka_cf_5'] != '') { echo '<span class="am_category_image"><i class="'.$revtheme_dop_menu['dop_menu_iconka_cf_5'].'"></i></span>'; } else { if ($revtheme_dop_menu['dop_menu_image_cf_5']) { echo '<span class="am_category_image"><img src="'.$revtheme_dop_menu['dop_menu_image_cf_5'].'" alt=""></span>'; } } ?><?php echo $revtheme_dop_menu['name'.$config_language_id]; ?>
													<?php } ?>
													</span>
													<?php if (isset($revtheme_dop_menu['children'])) { ?>
															<ul class="list-unstyled last_ul_cf">
																<?php foreach ($revtheme_dop_menu['children'] as $child) { ?>
																	<li>
																		<?php if ($child['href'.$config_language_id] != '') { ?>
																			<a href="<?php echo $child['href'.$config_language_id]; ?>"><?php if ($child['icontype'] == 'iconka' && $child['dop_menu_iconka_cf_5'] != '') { echo '<span class="am_category_image"><i class="'.$child['dop_menu_iconka_cf_5'].'"></i></span>'; } else { if ($child['dop_menu_image_cf_5']) { echo '<span class="am_category_image"><img src="'.$child['dop_menu_image_cf_5'].'" alt=""></span>'; } } ?><?php echo $child['name'.$config_language_id]; ?></a>
																		<?php } else { ?>
																			<?php if ($child['icontype'] == 'iconka' && $child['dop_menu_iconka_cf_5'] != '') { echo '<span class="am_category_image"><i class="'.$child['dop_menu_iconka_cf_5'].'"></i></span>'; } else { if ($child['dop_menu_image_cf_5']) { echo '<span class="am_category_image"><img src="'.$child['dop_menu_image_cf_5'].'" alt=""></span>'; } } ?><?php echo $child['name'.$config_language_id]; ?>
																		<?php } ?>
																	</li>
																<?php } ?>
															</ul>
													<?php } ?>
												</li>
											<?php } ?>
										</ul>
									<?php } ?>
									<?php if (strlen($cf_5_description) > 15) { echo $cf_5_description; } ?>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
				<?php } ?>
				<?php if ($revtheme_footer_icons) { ?>
					<?php $style_powered = 'text-align:right'; $class_powered = 'col-sm-6' ?>
					<div class="ficons col-sm-6 col-xs-12">
						<?php foreach ($revtheme_footer_icons as $revtheme_footer_icon) { ?>
							<span class="revtheme_footer_icon">
								<?php if (isset($revtheme_footer_icon['href']) && $revtheme_footer_icon['href'] != '') { ?>
									<a href="<?php echo $revtheme_footer_icon['href']; ?>">
										<img src="<?php echo $revtheme_footer_icon['image']; ?>" alt=""/>
									</a>
								<?php } else { ?>
									<img src="<?php echo $revtheme_footer_icon['image']; ?>" alt=""/>
								<?php } ?>
							</span>
						<?php } ?>
					</div>
				<?php } else { ?>
					<?php $style_powered = 'text-align:left'; $class_powered = 'col-sm-12' ?>
				<?php } ?>
				<div class="row">	
					<div style="<?php echo $style_powered; ?>" class="powered <?//php echo $class_powered; ?> col-xs-6"><?php echo "ООО «Мясо по госту» 2021"; ?></div>
					<div style="text-align:right; padding-right: 5%;" class="powered <?//php echo $class_powered; ?> col-xs-6"><?php echo "Разработка сайта: "; ?><a href="https://siteonic.ru/" target="_blank">Сайтоник</a></div>
				</div>
				</div>
		</div>
	</div>	
	<div class="container" style="position: relative;">
		<img src="/image/catalog/revolution/Ukrashenija/footer-one.png" class="img-responsive-background4">
		<img src="/image/catalog/revolution/Ukrashenija/45.png" class="img-responsive-background5">
	</div>
</footer>
<?php if (!$setting_all_settings['all_document_width'] && !$setting_all_settings['all_document_h_f_width']) { ?>
</div>
<?php } ?>
</div>
<?php if ($popup_phone) { ?>
	<div class="popup-phone-wrapper" data-toggle="tooltip" data-placement="left"  title="<?php echo $text_footer_popup_phone_tooltip; ?>">
		<span class="scroll-top-inner">
			<i class="fa fa-phone"></i>
		</span>
	</div>
<?php } ?>
<?php if ($in_top) { ?>
	<div class="scroll-top-wrapper ">
		<span class="scroll-top-inner">
			<i class="fa fa-arrow-circle-up"></i>
		</span>
	</div>
<?php } ?>
<?php if (!$setting_all_settings['minif_on']) { ?>
	<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js"></script>
	<script src="catalog/view/javascript/revolution/common.js"></script>
	<?php if ($setting_all_settings['all_document_snow_on']) { ?>
		<script src="catalog/view/javascript/revolution/snow.min.js"></script>
	<?php } ?>
	<?php if ($setting_all_settings['mobile_header'] == '2') { ?>
		<script src="catalog/view/javascript/revolution/jquery.mmenu.js"></script>
	<?php } ?>
	<script src="catalog/view/javascript/revolution/js_change.js"></script>
<?php } else { ?>
	<script src="catalog/view/javascript/revolution/javascript_min.js"></script>
<?php } ?>
<script><!--
	<?php if ($revtheme_filter['status']) { ?>
		(function($){
			$.fn.revFilter = function(f) {
				var g = this.selector;
				var h = $(g).attr('action');
				$(document).ready(function() {
					init_revfilter();
				});
				$(document).on('submit', g, function(e) {
					e.preventDefault();
					var a = $(this).serialize();
					loadProds(h,a,f.revload);
				});
				$(document).on('click', '#'+f.reset_id, function(e) {
					$(g+' input, '+g+' select').not('[type=hidden]').each(function(a) {
						if ($(this).hasClass('irs-hidden-input')) {
							var b = $(this).data('ionRangeSlider');
							b.reset();
							}
						if ($(this).is(':checkbox') || $(this).is(':radio')) {
							$(this).removeAttr("checked");
						} else {
							$(this).val('');
						}
					});
					var c = $(g).serialize();
					loadProds(h,c,f.revload);
				});
				if (f.mode == 'auto') {
					$(document).on('change', g+' input:not([type=hidden]):not(.irs-hidden-input), '+g+' select', function() {
						$(g).submit();
					})
				}
				function init_revfilter() {
					<?php if ($setting_catalog_all['pagination'] == 'knopka') { ?>
						$('.pagpages').addClass('dnone');
					<?php } ?>
					<?php if ($setting_catalog_all['pagination'] == 'standart_knopka' || $setting_catalog_all['pagination'] == 'knopka') { ?>
						var a = $('#load_more').html();
						$('.pagination').parent().parent().before(a);
					<?php } ?>
					$('#input-sort').removeAttr('onchange');
					$('#input-limit').removeAttr('onchange');
					$(f.selector).addClass('revcontainer');
					if (localStorage.getItem('display') == 'list') {
						list_view();
					} else if (localStorage.getItem('display') == 'price') {
						price_view();
					} else if (localStorage.getItem('display') == 'grid') {
						grid_view();
					} else {
						<?php if ($setting_catalog_all['vid_default'] == 'vid_price') { ?>
							price_view();
						<?php } else if ($setting_catalog_all['vid_default'] == 'vid_list') { ?>
							list_view();
						<?php } else if ($setting_catalog_all['vid_default'] == 'vid_grid') { ?>
							grid_view();
						<?php } ?>
					}
					<?php if ($setting_catalog_all['img_slider']) { ?>
						$('#content .owlproduct').owlCarousel( {
							beforeInit: true,
							items: 1,
							singleItem: true,
							mouseDrag: false,
							autoPlay: false,
							navigation: true,
							navigationText: ['<i class="fa fa-chevron-left fa-3x"></i>','<i class="fa fa-chevron-right fa-3x"></i>'],
							pagination: false
						});
						<?php if (!$setting_catalog_all['chislo_ryad']) { ?>
							if (localStorage.getItem('display')=='grid') {
								$('.product-thumb > .image').css('width','initial');
							}
						<?php } ?>
						podgon_img();
					<?php } else { ?>
						$('.owl-carousel.owlproduct').remove();
					<?php } ?>
					podgon_fona();
					$('#column-left #revfilter_box .mobil_wellsm .well.well-sm').remove();
					if ($(window).width() < 991) {
						$('#column-left #revfilter_box .mobil_wellsm .collapsible').append($('.revfilter_container > .well.well-sm'));
					}
					<?php echo(isset($revtheme_filter['scripts']) ? $revtheme_filter['scripts'] : ''); ?>
				}
				function loadProds(c,d,e) {
					d = d || '';
					e = e || false;
					filterurl = c + '&isrevfilter=1';
					$.ajax({
						url: filterurl,
						type: 'get',
						data: d,
						processData: false,
						dataType: e ? 'json' : 'html',
						beforeSend: function() {
							$(g+' button').button('loading');
							masked('.products_category > .product-layout > .product-thumb',true);
							$('.load_more .fa-refresh').addClass('fa-spin');
						},
						success: function(a) {
							var b = $.parseHTML((e && (typeof a.html != 'undefined')) ? a.html : a);
							$(f.selector).children().remove();
							$(f.selector).append($(b).find(f.selector).children());
							<?php if (isset($revtheme_filter['filter_recount']) && $revtheme_filter['filter_recount']) { ?>
								if(typeof a.filters != 'undefined') {
									reloadFilter(a.filters);
								}
							<?php } ?>
							init_revfilter();
						},
						complete: function() {
							setTimeout(function() {
								masked('.products_category > .product-layout > .product-thumb',false);
								autoscroll_loading = false;
								$(g+' button').button('reset');
								var pr_opts_cat = $('.products_category .options_buy')
								pr_opts_cat.find('select:first').each(function() {
									this.onchange();
								});
							},250);
							if (f.mode == 'manual' && $(window).width() > 767) {
								element = $('.breadcrumb');
								offset = element.offset();
								offsetTop = offset.top;
								//$('html, body').animate({scrollTop:offsetTop}, 250, 'linear');
							};
							$('.load_more .fa-refresh').removeClass('fa-spin').css('hover');
							<?php if ($setting_catalog_all['pagination'] == 'auto') { ?>
								$('.pagpages .pagination').hide();
							<?php } ?>
							<?php if (isset($revtheme_filter['filter_brstroka']) && $revtheme_filter['filter_brstroka']) { ?>
								var urlfull = c + (d ? ((c.indexOf('?') > 0 ? '&' : '?') + d) : '');
								urlfull = decodeURIComponent(urlfull);
								history.pushState('', '', urlfull);
							<?php } ?>
						}
					})
				}
				<?php if (isset($revtheme_filter['filter_recount']) && $revtheme_filter['filter_recount']) { ?>
					function reloadFilter(filters) {
						jQuery.each(f.filter_data, function(id, values) {
							var group = $('#' + id);
							if(typeof filters[id] == 'undefined') {
								group.addClass('not-active');
							}
							jQuery.each(values, function(i, val_id) {
								var val = $('#' + val_id);
								if(typeof filters[id] != 'undefined' && typeof filters[id][val_id] != 'undefined') {
									val.fadeTo('fast', 1);
									val.find('input').prop('disabled', false);
									<?php if ($revtheme_filter['filter_count_products'] && (isset($revtheme_filter['filter_recount']) && $revtheme_filter['filter_recount'])) { ?>
										val.find('.revlabel').html(filters[id][val_id]['count']);
									<?php } ?>
								} else {
									val.fadeTo('slow', 0.5);
									val.find('input').prop('disabled', true);
									<?php if ($revtheme_filter['filter_count_products'] && (isset($revtheme_filter['filter_recount']) && $revtheme_filter['filter_recount'])) { ?>
										val.find('.revlabel').html('0');
									<?php } ?>
								}
							});
						});
					}
				<?php } ?>
				$(document).on('click', '.pagination a', function(e) {
					loadProds($(this).attr('href'), null, true);
					element = $('.breadcrumb');
					offset = element.offset();
					offsetTop = offset.top;
					$('html, body').animate({scrollTop:offsetTop}, 250, 'linear');
					return false;
				});
				$(document).on('change', '#input-sort', function(e) {
					var a = $(this).val();
					sort = a.match('sort=([A-Za-z.]+)');
					$('input[name="sort"]').val(sort[1]);
					order = a.match('order=([A-Z]+)');
					$('input[name="order"]').val(order[1]);
					$(g).submit();
				});
				$(document).on('change', '#input-limit', function(e) {
					var a = $(this).val();
					if (a) {
						limit = a.match('limit=([0-9]+)');
						$('input[name="limit"]').val(limit[1]);
					}
					$(g).submit();
				});
				<?php if ($setting_catalog_all['pagination'] == 'standart_knopka' || $setting_catalog_all['pagination'] == 'knopka') { ?>
					var i = $('#input-limit').val();
					if (i) {
						limit = i.match('limit=([0-9]+)');
						$i = limit[1];
					}
					$(document).on('click', '.load_more', function(e) {
						e.preventDefault();
						var a = $('#input-limit').val();
						if (a) {
							limit = a.match('limit=([0-9]+)');
						}
						limit3 = $('#revfilter input[name="limit"]').val();
						if (limit3) {
							limit21 = limit3;
						} else {
							limit21 = limit[1];
							$('#revfilter input[name="limit"]').val(limit21);
						}
						limit2 = Number(limit21)+Number($i);
						limitnumber = 'limit='+limit21;
						a = a.replace('limit='+$i,'');
						a = a.replace(limitnumber,'');
						var b = a+'limit='+limit2;
						$('#revfilter input[name="limit"]').val(limit2);
						$(g).submit();
					});
				<?php } ?>
				<?php if ($setting_catalog_all['pagination'] == 'auto') { ?>
					var i = $('#input-limit').val();
					limit = i.match('limit=([0-9]+)');
					$i = limit[1];
					autoscroll_loading = false;
					$('.pagpages .pagination').hide();
					$(window).scroll(function() {
						if (inZone('.pagpages') && !autoscroll_loading) {
							autoscroll_loading = true;
							var c = $(".pagpages .pagination li.active").next("li").children("a");
							if (c.length==0) return;
							setTimeout(function() {
								var a = $('#input-limit').val();
								limit = a.match('limit=([0-9]+)');
								limit3 = $('#revfilter input[name="limit"]').val();
								if (limit3) {
									limit21 = limit3;
								} else {
									limit21 = limit[1];
									$('#revfilter input[name="limit"]').val(limit21);
								}
								limit2 = Number(limit21)+Number($i);
								limitnumber = 'limit='+limit21;
								a = a.replace('limit='+$i,'');
								a = a.replace(limitnumber,'');
								var b = a+'limit='+limit2;
								$('#revfilter input[name="limit"]').val(limit2);
								$(g).submit();
							}, 250);
						}
					});
				<?php } ?>
				function inZone(a) {
					if ($(a).length) {
					var b = $(window).scrollTop();
					var c = $(window).height();
					var d = $(a).offset();
					if (b<=d.top&&($(a).height()+d.top)<(b+c)) return true
					};
					return false;
				}
				$(document).on('click','#list-view',function() {
					list_view();
				});
				$(document).on('click', '#grid-view', function() {
					grid_view();
				});
				$(document).on('click', '#price-view', function() {
					price_view();
				});
			}
		})(jQuery);
	<?php } ?>
	<?php if (!$revtheme_filter['status'] || $revfilter_route) { ?>
		<?php if ($setting_catalog_all['pagination'] != 'standart') { ?>
			<?php if ($setting_catalog_all['pagination'] == 'standart_knopka') { ?>
			var button_more = true; var pagination_exist = true; var autoscroll = false;
			<?php } else if ($setting_catalog_all['pagination'] == 'auto') { ?>
			var button_more = false; var pagination_exist = true; var autoscroll = true;
			<?php } else { ?>
			var button_more = true; var pagination_exist = false; var autoscroll = false;
			<?php } ?>
			var window_height = 0; var product_block_offset = 0; var product_block = '.row > .product-layout'; var pages_count = 0; var pages = [];
			function gettNextProductPage(pages, pages_count) {
				if (pages_count >= pages.length) return;
				masked('.row > .product-layout > .product-thumb', true);
				$.ajax({
					url:pages[pages_count], 
					type:"GET", 
					data:'',
					beforeSend: function(){
						$('.load_more .fa-refresh').addClass('fa-spin');
					},
					success:function (data) {
						$data = $(data);
						masked('.row > .product-layout > .product-thumb', false);
						$data.find('.row > .product-layout > .product-thumb').addClass('op_dblock1');
						if ($data) {
							if (localStorage.getItem('display') == 'list') {
								$(product_block).parent().append($data.find('#content .product-layout').parent().html());
								list_view();
								if (product_block == '.product-grid') {grid_view();};
							} else if (localStorage.getItem('display') == 'price') {
								$(product_block).parent().append($data.find('#content .product-layout').parent().html());
								price_view();
							} else {
								$(product_block).parent().append($data.find('#content .product-layout').parent().html());
								grid_view();
							}
							if (pagination_exist) {
								$('.pagination').html($data.find('.pagination'));
							}
							$('.load_more .fa-refresh').removeClass('fa-spin').css('hover');
							if (pages_count+1 >= pages.length) {$('.load_more').hide();};
							setTimeout(function() {
								$('.row > .product-layout > .product-thumb').removeClass('op_dblock1').addClass('op_dblock2');
							}, 220)
						}
					}
				});
			}
			$(document).ready(function(){
				window_height = $(window).height();
				var button_more_block = $('#load_more').html();
				if ($(product_block).length > 0) {
					product_block_offset = $(product_block).offset().top;
					var href = $('.pagination').find('li:last a').attr('href');
					$('.pagination').each(function(){
						if (href) {
							TotalPages = href.substring(href.indexOf("page=")+5);
							First_index = $(this).find('li.active span').html();
							i = parseInt(First_index) + 1;
							while (i <= TotalPages) {
								pages.push(href.substring(0,href.indexOf("page=")+5) + i);
								i++;
							}
						}		
					});	
					if (button_more && href) {
						$('.pagination').parent().parent().before(button_more_block);
						if (!pagination_exist) {
							$('.pagpages').addClass('dnone');
						}
						$(".load_more").click(function(event) {
							event.preventDefault();
							gettNextProductPage(pages, pages_count);
							pages_count++;
							setTimeout(function() {
								if (pages_count > 0) {
									var $next = $(".pagpages .pagination li.active").next("li").children("a");
									if ($next.length == 0) return;
									$.get($next.attr("href"), function(data) {
										$data = $(data);
										var pag = $data.find(".pagpages > *");
										var pag2 = pag.filter(".text-right").html();
										var pag_ch =  pag2.substr(0,pag2.indexOf(" по")).replace(/[^\d.]/ig, '');
										pag2 = pag2.replace(pag_ch, "1");
										$(".pagpages").html(pag);
										$(".pagpages .text-right").html(pag2);
										$(".owlproduct").not(".owl-theme").owlCarousel({
											beforeInit: true,
											items: 1,
											singleItem: true,
											mouseDrag: false,
											autoPlay: false,
											navigation: true,
											navigationText: ['<i class="fa fa-chevron-left fa-3x"></i>', '<i class="fa fa-chevron-right fa-3x"></i>'],
											pagination: false
										});
									}, "html")
								}
							}, 350);
						});
					} else if (autoscroll) {
						$('.pagpages .pagination').hide();
						autoscroll_loading = false;
						$(window).scroll(function() {
							if(inZone('.pagpages') && !autoscroll_loading) {
								autoscroll_loading = true;
								setTimeout(function() {
									gettNextProductPage(pages, pages_count);
									pages_count++;
									if (pages_count > 0) {
										var $next = $(".pagpages .pagination li.active").next("li").children("a");
										if ($next.length == 0) return;
										$.get($next.attr("href"), function(data) {
											$data = $(data);
											var pag = $data.find(".pagpages > *");
											var pag2 = pag.filter(".text-right").html();
											var pag_ch =  pag2.substr(0,pag2.indexOf(" по")).replace(/[^\d.]/ig, '');
											pag2 = pag2.replace(pag_ch, "1");
											$(".pagpages .text-right").html(pag2);
											$(".owlproduct").not(".owl-theme").owlCarousel({
												beforeInit: true,
												items: 1,
												singleItem: true,
												mouseDrag: false,
												autoPlay: false,
												navigation: true,
												navigationText: ['<i class="fa fa-chevron-left fa-3x"></i>', '<i class="fa fa-chevron-right fa-3x"></i>'],
												pagination: false
											});
											autoscroll_loading = false;
										}, "html")
									}
								}, 350);
							}
						});
					}
				}
			});
			function inZone(el){
				if($(el).length) {
					var scrollTop = $(window).scrollTop();
					var windowHeight = $(window).height();
					var offset = $(el).offset();
					if(scrollTop <= offset.top && ($(el).height() + offset.top) < (scrollTop + windowHeight))
					return true;
				};
				return false;
			}
		<?php } ?>
	<?php } ?>
//--></script>
<?php if ($user_scripts) { ?>
	<?php echo $user_scripts; ?>
<?php } ?>
<?php if ($setting_catalog_all['pagination'] != 'standart') { ?>
	<div id="load_more" style="display:none;"><div class="col-sm-12 text-center"><a href="#" class="load_more btn btn-primary" rel="nofollow"><i class="fa fa-refresh"></i><?php echo $text_loadmore; ?></a></div></div>
<?php } ?>
<?php if ($setting_all_settings['cookies']) { ?>
	<!--noindex-->
	<div class="bottom_cookie_block">
		<span><?php echo $cookies_text; ?></span>
		<a href="javascript:void(0);" class="bottom_cookie_block_ok btn-sm btn-primary">Ok</a>
	</div>
	<script><!--
		var Cookie = {
			set: function(name, value, days) {
				var domain, domainParts, date, expires, host;
				if (days) {
					date = new Date();
					date.setTime(date.getTime()+(days*24*60*60*1000));
					expires = "; expires="+date.toGMTString();
				} else {
					expires = "";
				}
				host = location.host;
				if (host.split('.').length === 1) {
					document.cookie = name+"="+value+expires+"; path=/";
				} else {
					domainParts = host.split('.');
					domainParts.shift();
					domain = '.'+domainParts.join('.');
					document.cookie = name+"="+value+expires+"; path=/";
					if (Cookie.get(name) == null || Cookie.get(name) != value) {
						domain = '.'+host;
						document.cookie = name+"="+value+expires+"; path=/";
					}
				}
				return domain;
			},
			get: function(name) {
				var nameEQ = name + "=";
				var ca = document.cookie.split(';');
				for (var i=0; i < ca.length; i++) {
					var c = ca[i];
					while (c.charAt(0)==' ') {
						c = c.substring(1,c.length);
					}
					if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
				}
				return null;
			}
		};
		if(!Cookie.get('revcookie')) {
			setTimeout("document.querySelector('.bottom_cookie_block').style.display='block'", 500);
		}
		$('.bottom_cookie_block_ok').click(function(){
			$('.bottom_cookie_block').fadeOut();
			Cookie.set('revcookie', true, '120');
		});
	//--></script>
	<!--/noindex-->
<?php } ?>
</body></html>