<div id="cart_mobi" class="btn-group show-on-hover w100 <?php echo $mini_header_cart_class; ?>">
	<?php if ($setting_header_cart['type'] == 'redirect') { ?>
		<button class="cart" onclick="get_revpopup_cart('', 'redirect_cart', '');">
			<span id="cart-total_mobi"><?php echo $text_items; ?></span>
		</button>
	<?php } else { ?>
		<button class="cart" onclick="get_revpopup_cart('', 'show_cart', '');">
			<span id="cart-total_mobi"><?php echo $text_items; ?></span>
		</button>
	<?php } ?>
</div>