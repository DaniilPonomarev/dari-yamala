<?php echo $header; ?>
<div id="top5" class="clearfix">
<?php if ($slideshow) { ?>
<div id="top4" class="clearfix">
	<?php if (($setting_home_slideshow['allwide'] && !$amazon) || ($setting_home_slideshow['allwide'] && $amazon && !$allwide)) { ?>
	<?php } else { ?>
	<div class="container">
		<div class="row">
		<?php } ?>
			<?php if ($allwide && $amazon) { ?>
				
				<div class="hidden-xs hidden-sm col-md-3"></div>
				<div class="col-sm-12 col-md-9"><?php echo $slideshow; ?></div>
				<?php if ($h1_home) { ?>
					<div class="col-md-offset-3"><h1 class="home_h1"><?php echo $h1_home; ?></h1></div>
				<?php } ?>
			<?php } else { ?>
				
				<div class="col-sm-12 col-md-12"><?php echo $slideshow; ?></div>
				
			<?php } ?>
		<?php if (($setting_home_slideshow['allwide'] && !$amazon) || ($setting_home_slideshow['allwide'] && $amazon && !$allwide)) { ?>
		<?php } else { ?>
		</div>
	</div>
	<?php } ?>
</div>
<?php if ($h1_home) { ?>
					<h1 class="home_h1"><?php echo $h1_home; ?></h1>
				<?php } ?>	
<?php } else { ?>
	<?php if ($h1_home && $is_desctope) { ?>
		<div class="container">
			<div class="row">
				<?php if ($allwide && $amazon) { ?>
				<div class="hidden-xs hidden-sm col-md-offset-3"><h1 class="home2_h1"><?php echo $h1_home; ?></h1></div>
				<?php } else { ?>
				<div class="hidden-xs hidden-sm"><h1 class="home2_h1"><?php echo $h1_home; ?></h1></div>
				<?php } ?>
			</div>
		</div>
	<?php } ?>
<?php } ?>
<div class="container" style="padding-top: 2%;">
	<div class="row">
	<?php echo $column_left; ?>
	<?php if ($column_left && $column_right) { ?>
	<?php $class = 'col-sm-6'; ?>
	<?php } elseif ($column_left || $column_right) { ?>
	<?php $class = 'col-sm-9'; ?>
	<?php } else { ?>
	<?php $class = 'col-sm-12'; ?>
	<?php } ?>
	
	<div id="content" class="<?php echo $class; ?>">
	
	<?php echo $content_top; ?>
	
	<?php echo $revcategorywall; ?>
	<?php echo $ptabs; ?>
	<?php echo $pbest; ?>
	<?php echo $pspec; ?>
	<?php echo $plast; ?>

	<?php echo $slider_tabs; ?>

	<?php echo $slider_1; ?>

	<?php echo $slider_2; ?>

	<?php echo $slider_3; ?>

	<?php echo $slider_4; ?>

	<?php if ($blocks) { ?>
		<div id="top6" class="clearfix">
			<div class="container" style="background: #101212;">
				<div class="row">
					<?php echo $blocks; ?>
				</div>
			</div>
		</div>	
		<?php } else { ?>
		<?php if ($allwide && $amazon) { ?>
		<div style="height:70px; width:100%; clear:both"></div>
		<?php } else { ?>
		<div style="height:30px; width:100%; clear:both"></div>
		<?php } ?>
		<?php } ?>
	</div>

	<?php if ($blog) { ?>
	</div>
	</div>
	</div>

	<div id="top7" class="clearfix">
	<div class="container">
	<?php echo $blog; ?>
	</div>
	</div>
	<div class="container">
	<div class="row">
	<div class="col-sm-12">
	<?php } ?>

	


	<?php if ($aboutstore) { ?>
	<?php if ($socv || $socfb || $socok || ($socvinsta && $right_insta)) {$aboutclass1="col-sm-12 col-md-8 col-lg-9 aboutstore";$aboutclass2="col-sm-12 col-md-4 col-lg-3";} else {$aboutclass1="col-sm-12 aboutstore2";$aboutclass2="";} ?>
	<div class="row">
	<div class="<?php echo $aboutclass1; ?>">
	<!--<div class="container" style="position: relative;">
		<img src="/image/catalog/revolution/Ukrashenija/image2.png" class="img-responsive-background">
		<img src="/image/catalog/revolution/Ukrashenija/28.png" class="img-responsive-background2">
	</div>-->
	<?php echo $aboutstore; ?>
	<?php if (!$right_insta) { ?>
	<?php echo $socvinsta; ?>
	<?php } ?>
	</div>
	<div class="<?php echo $aboutclass2; ?>">
	<?php echo $socv; ?>
	<?php echo $socfb; ?>
	<?php echo $socok; ?>
	<?php if ($right_insta) { ?>
	<?php echo $socvinsta; ?>
	<?php } ?>
	</div>
	</div>
	<?php } ?>
	
	<?php echo $storereview; ?>
	<?php echo $viewed_products; ?>
	<!--<div class="container" style="position: relative;">
		<img src="/image/catalog/revolution/Ukrashenija/27.png" class="img-responsive-background3">
	</div>-->
	<?php if (1==0) {?>
	<div id="wrapMap">
		<iframe style="pointer-events: none;" src="https://yandex.ru/map-widget/v1/?um=constructor%3A3150c4be386f563418b1b96f2fe0fcae7fd5c82a53cfa4e3844626586841809e&amp;source=constructor" width="100%" height="400" frameborder="0"></iframe>
	</div>
	<?php } ?>
	<?php echo $content_bottom; ?>
	
	</div>
	<?php echo $column_right; ?>
	</div>
</div>

<?php echo $footer; ?>
<script>
		// создаём элемент <div>, который будем перемещать вместе с указателем мыши пользователя
		var mapTitle = document.createElement('div'); mapTitle.className = 'mapTitle';
		// вписываем нужный нам текст внутрь элемента
		mapTitle.textContent = 'Для активации карты нажмите по ней';
		// добавляем элемент с подсказкой последним элементов внутрь нашего <div> с id wrapMap
		wrapMap.appendChild(mapTitle);
		// по клику на карту
		wrapMap.onclick = function() {
			// убираем атрибут "style", в котором прописано свойство "pointer-events"
			this.children[0].removeAttribute('style');
			// удаляем элемент с интерактивной подсказкой
			mapTitle.parentElement.removeChild(mapTitle);
		}
		// по движению мыши в области карты
		wrapMap.onmousemove = function(event) {
			// показываем подсказку
			mapTitle.style.display = 'block';
			// двигаем подсказку по области карты вместе с мышкой пользователя
			if(event.offsetY > 10) mapTitle.style.top = event.offsetY + 20 + 'px';
			if(event.offsetX > 10) mapTitle.style.left = event.offsetX + 20 + 'px';
		}
		// при уходе указателя мыши с области карты
		wrapMap.onmouseleave = function() {
			// прячем подсказку
			mapTitle.style.display = 'none';
		}
	</script>